const aws = require('aws-sdk');
const fs = require('fs');

const bucketName = 'new-backend-file-storage-bucket';
// const bucketName = process.env.AWS_BUCKET_NAME;
const bucketRegion = process.env.AWS_BUCKET_REGION;
const accessKeyId = process.env.AWS_ACCESS_KEY_ID;
const secretKey = process.env.AWS_SECRET_KEY;

const s3 = new aws.S3({
  accessKeyId,
  secretKey,
  bucketRegion,
});

// function that upload files to s3
exports.uploadFile = async (file) => {
  const fileStream = fs.createReadStream(file.path);

  const uploadParams = {
    Bucket: bucketName,
    Body: fileStream,
    Key: file.filename,
  };

  return s3.upload(uploadParams).promise();
};

// function that download files to s3
// Get it done later
exports.getFile = (fileKey) => {
  const downloadParams = {
    Bucket: bucketName,
    Key: fileKey,
  };

  return s3.getObject(downloadParams).promise();
};

exports.listFiles = () => {
  const listParams = {
    Bucket: bucketName,
  };

  return s3.listObjects(listParams).promise();
  // return s3.listObjects(listParams).promise();
};

exports.deleteFile = (fileKey) => {
  const deleteParams = {
    Bucket: bucketName,
    Key: fileKey,
  };

  return s3.deleteObject(deleteParams).promise();
};
