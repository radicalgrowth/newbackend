// Importing
const express = require('express');
const cors = require('cors');
const morgan = require('morgan');

// Router importing
const dashboardRouter = require('./routes/dashboardRouter');
const deviceRouter = require('./routes/deviceRoutes');
const alertRouter = require('./routes/alertRoutes');
const chartRouter = require('./routes/chartRoutes');
const deviceAllocationRouter = require('./routes/deviceAllocationRoutes');
const userRouter = require('./routes/userRoutes');
const farmRouter = require('./routes/farmRoutes');
const userFarmRouter = require('./routes/userFarmRoutes');
const fieldRouter = require('./routes/fieldRoutes');
const cropRouter = require('./routes/cropRoutes');
const sensorRouter = require('./routes/sensorRoutes');
const soilRouter = require('./routes/soilRoutes');
const irrigationRouter = require('./routes/irrigationRoutes');
const pivotRouter = require('./routes/pivotRoutes');

// Initializing
const app = express();

// Middlewares
// Access-Control-Allow-Origin
app.use(cors());
app.options('*', cors());
app.use(express.json());
app.use(morgan('dev'));
/* var corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200,
}; */

// app.use('*', (req, res, next) => {
// console.log({ message: 'in middleware' });
/* req.headers['Access-Control-Allow-Origin'] = 'http://localhost:3000';
  req.headers['Access-Control-Allow-Methods'] = 'GET,PUT,POST,DELETE,OPTIONS';
  req.headers['Access-Control-Allow-Headers'] =
    'Content-Type, Accept, Access-Control-Allow-Origin, Authorization'; */
// res.header('Access-Control-Allow-Origin', '*');
// console.log({ headers: req.headers });

//   next();
// });

// API routes
app.get('/api/v1/', (req, res) =>
  res.status(200).json({ message: "Welcome to the Radical's API" })
);
app.use('/api/v1/dashboard', dashboardRouter);
app.use('/api/v1/devices', deviceRouter);
app.use('/api/v1/alerts', alertRouter);
app.use('/api/v1/charts', chartRouter);
app.use('/api/v1/deviceAllocation', deviceAllocationRouter);
app.use('/api/v1/users', userRouter);
app.use('/api/v1/farm', farmRouter);
app.use('/api/v1/userfarm', userFarmRouter);
app.use('/api/v1/field', fieldRouter);
app.use('/api/v1/crop', cropRouter);
app.use('/api/v1/sensor', sensorRouter);
app.use('/api/v1/soil', soilRouter);
app.use('/api/v1/irrigation', irrigationRouter);
app.use('/api/v1/pivot', pivotRouter);

// Catching errors other then these routes
app.get('*', (req, res) => {
  res.json({
    status: 'Fail',
    message: `Can't find ${req.originalUrl} on this server`,
  });
});

module.exports = app;
