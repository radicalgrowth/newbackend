FROM node:16 

WORKDIR /usr/newBackend/server
COPY package*.json ./
RUN npm install    
COPY . .

ENV PORT 8800 
EXPOSE 8800

CMD ["npm","run", "start"]