// Importing
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Defining Schema
const sensorSchema = new Schema({
  sensor_type: { type: String, required: true },
  device_id: { type: Schema.Types.ObjectId, ref: 'Device', required: true },
  timestamp: {
    type: Date,
    default: Date.now,
  },
});

// Defining Schema
const Sensor = mongoose.model('Sensor', sensorSchema);
module.exports = Sensor;
