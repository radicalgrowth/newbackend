// Importing
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Defining Schema
const sprinklerSchema = new Schema({
  flow_rate: { type: Number, required: true },
  actual_area: { type: Number },
  percent_irrigated: { type: Number },
  irrigation_id: {
    type: Schema.Types.ObjectId,
    ref: 'Irrigation',
    required: true,
  },
  archive: { type: Boolean, default: false },
  timestamp: {
    type: Date,
    default: Date.now,
  },
});

// Defining Schema
const Sprinkler = mongoose.model('Sprinkler', sprinklerSchema);
module.exports = Sprinkler;
