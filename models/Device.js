// Importing
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Defining Schema
// Device name should be unique and device_topic should be unique too for future case
const deviceSchema = new Schema({
  name: { type: String, required: true, unique: false },
  fields: [{ type: String, required: true }],
  device_type: { type: String, enum: ['ground', 'air'], default: 'ground' },
  field_id: { type: Schema.Types.ObjectId, ref: 'Field', required: true },
  application_name: { type: String, required: true },
  device_topic: { type: String, required: true },
  status: { type: String, enum: ['active', 'inactive'], default: 'inactive' },
  archive: { type: Boolean, default: false },
  device_location: {
    lat: { type: Number, default: 0 },
    long: { type: Number, default: 0 },
  },
  timestamp: {
    type: Date,
    default: Date.now,
  },
});

/* const deviceSchema = new Schema({
  device_id: { type: String, required: true },
  user_id: { type: Schema.Types.ObjectId, ref: 'User', required: true },
  location_latitude: { type: String, default: '0' },
  location_longitude: { type: String, default: '0' },
  fields: [{ type: String }],
  field_id: { type: Schema.Types.ObjectId, ref: 'Field', required: true },
  // device_eui: { type: String, required: true, unique: true },
  timestamp: {
    type: Date,
    default: Date.now,
  },
}); */

// Defining model
const Device = mongoose.model('Device', deviceSchema);
module.exports = Device;
