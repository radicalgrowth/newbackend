// Importing
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Defining Schema
const fieldSchema = new Schema({
  name: { type: String, required: true, unique: true },
  description: { type: String },
  farm_id: { type: Schema.Types.ObjectId, ref: 'Farm', required: true },
  location: [{ lat: { type: Number }, lng: { type: Number } }],
  archive: { type: Boolean, default: false },
  timestamp: {
    type: Date,
    default: Date.now,
  },
});

// Defining Schema
const Field = mongoose.model('Field', fieldSchema);
module.exports = Field;
