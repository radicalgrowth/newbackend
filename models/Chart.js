// Importing
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Defining Schema
var chartSchema = new Schema({
  device_id: { type: Schema.Types.ObjectId, ref: 'Device', required: true },
  parameters: [{ type: String, required: true }],
  chart_type: { type: String, required: true },
  timestamp: {
    type: Date,
    default: Date.now,
  },
});

// Defining model
const Chart = mongoose.model('Charts', chartSchema);
module.exports = Chart;
