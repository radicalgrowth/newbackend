// Importing
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Defining Schema
const cropSchema = new Schema({
  crop_type: { type: String, required: true },
  planting_date: { type: Date, default: Date.now },
  emergence_date: { type: Date, default: Date.now },
  previous_crop: { type: String },
  field_id: { type: Schema.Types.ObjectId, ref: 'Field', required: true },
  archive: { type: Boolean, default: false },
  timestamp: {
    type: Date,
    default: Date.now,
  },
});

// Defining Schema
const Crop = mongoose.model('Crop', cropSchema);
module.exports = Crop;
