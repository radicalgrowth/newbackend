// Importing
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Defining Schema
const farmSchema = new Schema({
  name: { type: String, required: true, unique: true },
  archive: { type: Boolean, default: false },
  //   user_id: { type: mongoose.Schema.ObjectId, ref: 'User', require: true },
  timestamp: {
    type: Date,
    default: Date.now,
  },
});

// Defining Schema
const Farm = mongoose.model('Farm', farmSchema);
module.exports = Farm;
