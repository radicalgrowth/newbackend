// Importing
const mongoose = require('mongoose');
const validator = require('validator');
const User = require('./User');
const Schema = mongoose.Schema;

// Defining Schema
const eventSchema = new Schema({
  title: { type: String },
  date: { type: String, default: '' },
  type: { type: String, enum: ['irrigation', 'custom'], required: true },
  custom_type_text: { type: String },
  value: { type: String, required: true },
  user_id: { type: Schema.Types.ObjectId, ref: 'User', required: true },
  archive: { type: Boolean, default: false },
  timestamp: {
    type: Date,
    default: Date.now,
  },
});

// Defining Schema
const Event = mongoose.model('Event', eventSchema);
module.exports = Event;
