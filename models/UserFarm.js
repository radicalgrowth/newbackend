// Importing
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Defining Schema
const userFarmSchema = new Schema({
  user_id: { type: Schema.Types.ObjectId, ref: 'User', required: true },
  farm_id: {
    type: Schema.Types.ObjectId,
    ref: 'Farm',
    required: true,
  },
  timestamp: {
    type: Date,
    default: Date.now,
  },
});

// Defining Schema
const UserFarm = mongoose.model('UserFarm', userFarmSchema);
module.exports = UserFarm;
