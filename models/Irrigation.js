// Importing
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Defining Schema
const irrigationSchema = new Schema({
  system_type: {
    type: String,
    required: true,
    enum: ['pivot', 'drip', 'sdi', 'sprinkler', 'furrow'],
  },
  field_id: {
    type: Schema.Types.ObjectId,
    ref: 'Field',
    required: true,
  },
  archive: { type: Boolean, default: false },
  timestamp: {
    type: Date,
    default: Date.now,
  },
});

// Defining Schema
const Irrigation = mongoose.model('Irrigation', irrigationSchema);
module.exports = Irrigation;
