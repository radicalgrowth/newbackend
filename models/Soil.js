// Importing
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Defining Schema
const soilSchema = new Schema({
  depth: { type: Number, default: 0 },
  clay: { type: Number, default: 0, min: 0, max: 100 },
  sand: { type: Number, default: 0, min: 0, max: 100 },
  silt: { type: Number, default: 0, min: 0, max: 100 },
  organic_matter: { type: Number, default: 0, min: 0, max: 80 },
  ph: { type: Number, min: 2, max: 10 },
  cec: { type: Number, min: 1, max: 200 },
  ec: { type: Number, min: 0, max: 40 },
  field_id: { type: Schema.Types.ObjectId, ref: 'Field', required: true },
  archive: { type: Boolean, default: false },
  timestamp: {
    type: Date,
    default: Date.now,
  },
});

// Defining Schema
const Soil = mongoose.model('Soil', soilSchema);
module.exports = Soil;

// for later user
/* soil_zone: { type: String, required: true },
source: { type: String },
soil_property: {
  type: String,
  enum: [
    'Sand',
    'LoamySand',
    'SandyLoam',
    'Loam',
    'SiltLoam',
    'Silt',
    'SandyClayLoam',
    'SandyClay',
    'SiltyClay',
    'Clay',
  ],
}, */
