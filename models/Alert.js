// Importing
const mongoose = require('mongoose');
const validator = require('validator');
const Schema = mongoose.Schema;

// Defining Schema
const alertSchema = new Schema({
  alert_name: { type: String, required: true },
  device_id: { type: Schema.Types.ObjectId, ref: 'Device', required: true },
  parameter: { type: String, default: '' },
  condition: { type: String, enum: ['lower', 'higher'], required: true },
  action: { type: String, enum: ['email', 'sms', 'whatsapp'], required: true },
  destination: {
    type: String,
    required: true,
    /* validate: {
      validator: function (val) {
        // check if val is a email address or phone number
        return (
          /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im.test(
            val
          ) || validator.isEmail(val)
        );
      },
    }, */
  },
  active: { type: Boolean, default: true },
  snooze: { type: String, required: true, default: 0, max: 5, min: 0 },
  sent: { type: Number, default: 0, min: 0, max: 5 },
  user_id: { type: Schema.Types.ObjectId, ref: 'User', required: true },
  archive: { type: Boolean, default: false },
  timestamp: {
    type: Date,
    default: Date.now,
  },
});

// Defining Schema
const Alert = mongoose.model('Alert', alertSchema);
module.exports = Alert;
