// Importing
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Defining Schema
const deviceAllocationSchema = new Schema({
  user_id: { type: Schema.Types.ObjectId, ref: 'User', required: true },
  farm_id: { type: Schema.Types.ObjectId, ref: 'Farm', required: true },
  devices: [{ type: String }],
});

// Defining Model
const DeviceAllocation = mongoose.model(
  'DeviceAllocation',
  deviceAllocationSchema
);
module.exports = DeviceAllocation;
