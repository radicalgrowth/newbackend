// Importing
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const validator = require('validator');
const Cryptr = require('cryptr');
const cryptr = new Cryptr(process.env.CRYPTR_SECRET || 'myTotalySecretKey');
const Schema = mongoose.Schema;

// Defining Schema
const userSchema = new mongoose.Schema({
  name: String,
  email: {
    type: String,
    required: true,
    unique: true,
    validate: [validator.isEmail, 'Invalid Email'],
  },
  password: { type: String, required: true, select: false },
  role: {
    type: String,
    enum: ['super_admin', 'admin', 'worker'],
    required: true,
  },
  gender: { type: String, enum: ['male', 'female', 'other'] },
  phone_number: {
    type: String,
  },
  phone_code: { type: String },
  city: {
    type: String,
    default: '',
    // validate: [validator.isAlpha, 'Invalid City'],
  },
  country: {
    type: String,
    default: '',
    // validate: [validator.isAlpha, 'Invalid Country'],
  },
  organization: { type: String, default: '' },
  photo: {
    type: String,
    default:
      'https://new-backend-file-storage-bucket.s3.us-east-2.amazonaws.com/default.jpg',
  },
  photo_name: { type: String, default: 'default.jpg' },
  archive: { type: Boolean, default: false },
  application_name: { type: String, default: '', required: true },
  user_id: { type: Schema.Types.ObjectId, ref: 'User' },
  timestamp: { type: Date, default: Date.now },
  tokens: [
    {
      token: {
        type: String,
        // required: true
      },
    },
  ],
});

// Methods for later use
/* userSchema.methods.appendToken = async function (token) {
  const user = this;
  user.tokens = user.tokens.concat({ token });
  await user.save();
  return token;
}; */

// Middlewares
/* userSchema.pre(
  'save',
  // Hash password
  async function (next) {
    if (!this.isModified('password')) return next();
    // this.password = await bcrypt.hash(this.password, 8);
    this.password = cryptr.encrypt(this.password);
    this.password_confirm = undefined;
    next();
  }
);

// Generate JWT token
// this will happen only when user will login
/* userSchema.pre("save", async function (next) {
  const user = this;
  const token = await jwt.sign({ _id: user._id }, process.env.JWT_SECRET);
  user.tokens = user.tokens.concat({ token });
  next();
}); */

/* userSchema.methods.addProfilePicture = async function (path) {
  // Generate an auth token for the user
  const user = this;
  user.profile_picture = path;
  await user.save();
  return user;
}; */

/* userSchema.methods.generateAuthToken = async function () {
  // Generate an auth token for the user
  const user = this;
  const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET);
  user.tokens = user.tokens.concat({ token });
  await user.save();
  return token;
}; */

userSchema.methods.comparePassword = (password, hashed) => {
  // return bcrypt.compare(password, hashed);
  const decryptPass = cryptr.decrypt(hashed);
  /* console.log({
    message: 'in compare password',
    simple: password,
    encrypted: hashed,
    decrypted: decryptPass,
    condition: password === decryptPass,
  }); */
  return password === decryptPass;
};

/* userSchema.statics.findByCredentials = async (email, password) => {
  try {
    const user = await User.findOne({ email })
      .select("+password")
      .populate("assignedTo", "name")
      .populate("disorder", "name")
      .populate({ path: "configurations", select: "status" });
    // console.log("USER", user)
    if (!user) {
      throw new Error("user not found");
    }
    const isPasswordMatch = await bcrypt.compare(password, user.password);
    if (!isPasswordMatch) {
      // console.log(isPasswordMatch)
      throw new Error("Invalid login credentials");
    }
    return user;
  } catch (err) {
    console.log(err);
  }
}; */

// Defining Model
const User = mongoose.model('User', userSchema);
module.exports = User;
