// Importing
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Defining Schema
const pivotSchema = new Schema({
  pivot_shape: {
    type: String,
    enum: ['circle', 'semi_circle'],
    required: true,
  },
  name: { type: String },
  flow_rate: { type: Number, required: true },
  pivot_length: { type: Number, required: true },
  full_run_time: { type: Number, required: true },
  irrigation_id: {
    type: Schema.Types.ObjectId,
    ref: 'Irrigation',
    required: true,
  },
  archive: { type: Boolean, default: false },
  timestamp: {
    type: Date,
    default: Date.now,
  },
});

// Defining Schema
const Pivot = mongoose.model('Pivot', pivotSchema);
module.exports = Pivot;
