// Importing
const mongoose = require('mongoose');
const dotenv = require('dotenv');

process.on('uncaughtException', (err) => {
  console.log('UNCAUGHT EXCEPTION! Shuttıng down...');
  console.log(err.name, err.message);
  console.log(err);
  process.exit(1);
});

// Middlewares
dotenv.config({ path: './config.env' });
const app = require('./app'); // import here because of dotenv structure

// DB config
// Live DB
// const DBString = process.env.DB_LIVE.replace('<password>', process.env.DB_PASS);
const DBString = process.env.DB_LIVE;
// Local DB
// const DBString = process.env.DB_LOCAL;

mongoose
  .connect(DBString, { dbName: process.env.DB_NAME, useNewUrlParser: true })
  .then(() => console.log('DB connected'));

// Server listening
const port = process.env.PORT || 5000;
const server = app.listen(port, () =>
  console.log(`Server listening on port: ${port}`)
);

process.on('unhandledRejection', (err) => {
  console.log('UNHANDLER REJECTION! Shuttıng down...');
  console.log(err.name, err.message);
  console.log(err);
  server.close(() => {
    process.exit(1);
  });
});

process.on('SIGTERM', () => {
  console.log('SIGTERM RECEIVED. Shutting down gracefully');
  server.close(() => {
    console.log('Process terminated!');
  });
});
