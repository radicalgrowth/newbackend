// Importing
const express = require('express');
const router = express.Router();
const userController = require('./../controllers/userController');
const { body, validationResult } = require('express-validator');

// Authentication
router.post(
  '/signup',
  body('password').custom((value, { req, res }) => {
    if (value !== req.body.password_confirm) {
      return res.status(400).json({ message: 'Passwords do not match' });
    }
    return true;
  }),
  userController.hashPassword,
  userController.signup
);
router.post('/login', userController.login);

// Forgot password
router.post('/forgot-password', userController.forgotPassword);

// Create User and upload file
router
  .route('/')
  .post(
    userController.uploadUserPhoto,
    userController.resizeUserPhoto,
    userController.hashPassword,
    userController.createUser
  );

router.get('/all/:user_id', userController.getUserWorkers);

// Factory functions
router.route('/').get(userController.getAllUsers);
// .post(userController.createUser);

router
  .route('/:id')
  .get(userController.getUser)
  .patch(
    userController.uploadUserPhoto,
    userController.resizeUserPhoto,
    // userController.hashPassword,
    userController.updateUser
  )
  .delete(userController.deleteUser);

router.delete(
  '/permanent-delete/:user_id',
  userController.deleteUserPermanently
);

module.exports = router;
