const express = require('express');
const router = express.Router();
const dashboardController = require('../controllers/dashboardController');

// Get dashboard stats
router.get('/stats/:user_id', dashboardController.getDashboardStats);

// Events routes
router.get('/events/user-events/:user_id', dashboardController.getUserEvents);

// Events factory handler routes
router
  .route('/events')
  .get(dashboardController.getAllEvents)
  .post(dashboardController.createEvent);

router
  .route('/events/:id')
  .get(dashboardController.getEvent)
  .patch(dashboardController.updateEvent)
  .delete(dashboardController.deleteEvent);

router.delete(
  '/events/permanent-delete/:id',
  dashboardController.deletePermanently
);

module.exports = router;
