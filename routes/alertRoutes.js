const express = require('express');
const router = express.Router();
const alertController = require('../controllers/alertController');

// Get all alerts for a user
router.get('/get-user-alerts/:user_id', alertController.getAlertsByUserId);

router.get('/user-sent-alerts/:user_id', alertController.getUserUsedAlerts);

// Factory Handler routes
router
  .route('/')
  .get(alertController.getAllAlerts)
  .post(alertController.createAlert);

router
  .route('/:id')
  .get(alertController.getAlert)
  .patch(alertController.updateAlert)
  .delete(alertController.deleteAlert);

router.delete('/permanent-delete/:id', alertController.deletePermanently);

module.exports = router;
