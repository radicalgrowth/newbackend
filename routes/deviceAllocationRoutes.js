// Importing
const express = require("express");
const router = express.Router();
const deviceAllocationController = require("./../controllers/devicAllocationController");

router
  .route("/")
  .get(deviceAllocationController.getAllDeviceAllocation)
  .post(deviceAllocationController.createDeviceAllocation);

router
  .route("/:id")
  .get(deviceAllocationController.getDeviceAllocation)
  .patch(deviceAllocationController.updateDeviceAllocation)
  .delete(deviceAllocationController.deleteDeviceAllocation);

module.exports = router;
