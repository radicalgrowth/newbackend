// Importing
const express = require('express');
const router = express.Router();
const irrigationController = require('../controllers/irrigationController');

// Factory functions
router
  .route('/')
  .get(irrigationController.getAllIrrigations)
  .post(irrigationController.createIrrigation);

router
  .route('/:id')
  .get(irrigationController.getIrrigation)
  .patch(irrigationController.updateIrrigation)
  .delete(irrigationController.deleteIrrigation);

// Pivot Routes
router
  .route('/:IrrId/pivot')
  .get(irrigationController.getAllPivots)
  .post(irrigationController.createPivot);

router
  .route('/:IrrId/pivot/:id')
  .get(irrigationController.getPivot)
  .patch(irrigationController.updatePivot)
  .delete(irrigationController.deletePivot);

// Drip Routes
router
  .route('/:IrrId/drip')
  .get(irrigationController.getAllDrips)
  .post(irrigationController.createDrip);

router
  .route('/:IrrId/drip/:id')
  .get(irrigationController.getDrip)
  .patch(irrigationController.updateDrip)
  .delete(irrigationController.deleteDrip);

// Drip Routes
router
  .route('/:IrrId/sprinkler')
  .get(irrigationController.getAllSprinkler)
  .post(irrigationController.createSprinkler);

router
  .route('/:IrrId/sprinkler/:id')
  .get(irrigationController.getSprinkler)
  .patch(irrigationController.updateSprinkler)
  .delete(irrigationController.deleteSprinkler);

// Furrow Routes
router
  .route('/:IrrId/furrow')
  .get(irrigationController.getAllFurrows)
  .post(irrigationController.createFurrow);

router
  .route('/:IrrId/furrow/:id')
  .get(irrigationController.getFurrow)
  .patch(irrigationController.updateFurrow)
  .delete(irrigationController.DeleteFurrow);

module.exports = router;
