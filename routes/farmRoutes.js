// Importing
const express = require('express');
const router = express.Router();
const farmController = require('../controllers/farmController');

// Get all farms by a user id
router.get('/user-farms/:user_id', farmController.getAllUserFarms);

router.delete('/permanent-delete/:id', farmController.permanentDelete);

// Factory functions
router
  .route('/')
  .get(farmController.getAllFarms)
  .post(farmController.createFarm);

router
  .route('/:id')
  .get(farmController.getFarm)
  .patch(farmController.updateFarm)
  .delete(farmController.deleteFarm);

module.exports = router;
