// Importing
const express = require('express');
const router = express.Router();
const soilController = require('../controllers/soilController');

// Factory functions
router
  .route('/')
  .get(soilController.getAllSoils)
  .post(soilController.createSoil);

router
  .route('/:id')
  .get(soilController.getSoil)
  .patch(soilController.updateSoil)
  .delete(soilController.deleteSoil);

module.exports = router;
