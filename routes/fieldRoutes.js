// Importing
const express = require('express');
const router = express.Router();
const fieldController = require('../controllers/fieldController');

// Get all fields from a farm id
router.route('/farm-fields/:farm_id').get(fieldController.getAllFarmFields);

// Create field and all other parameters. not a factory function
router.post('/', fieldController.createField);

router.get('/:id', fieldController.getField);

// Get all fields by a user
router.get('/user-fields/:user_id', fieldController.getAllUserFields);

router.delete('/permanent-delete/:id', fieldController.permanentDelete);

// Factory functions
router.route('/').get(fieldController.getAllFields);
// .post(fieldController.createField);
router
  .route('/:id')
  .patch(fieldController.updateField)
  .delete(fieldController.deleteField);

module.exports = router;
