// Importing
const express = require('express');
const router = express.Router();
const sensorController = require('../controllers/sensorController');

// Factory functions
router
  .route('/')
  .get(sensorController.getAllSensor)
  .post(sensorController.createSensor);

router
  .route('/:id')
  .get(sensorController.getSensor)
  .patch(sensorController.updateSensor)
  .delete(sensorController.deleteSensor);

module.exports = router;
