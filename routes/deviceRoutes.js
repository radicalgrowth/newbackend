const express = require('express');
const router = express.Router();
const deviceController = require('./../controllers/deviceController');

// Thingstack routes
router.get('/get-thingstack-devices', deviceController.getThingStackDevices);
router.get('/get-thingstack-fields', deviceController.getThingStackFields);
router.get('/get-user-devices/:user_id', deviceController.getDevicesByUserId);
// router.delete('/:user_id', deviceController.deleteDeviceByUserId);
router.post('/get-device-data', deviceController.getDeviceDataById);
router.post(
  '/get-multiple-devices-data',
  deviceController.getMultipleDeviceData
);
// router.post('/get-user-devices-data', deviceController.getAllDevicesData);
router.get(
  '/get-device-parameters/:id',
  deviceController.getDeviceParametersById
);
router.post('/check-deviceid', deviceController.checkDeviceAlreadyRegistered);
router.post('/get-device-last-data', deviceController.getDeviceLastDataById);

/* router.get(
  '/get-device-latest-data/:device_id',
  deviceController.getDeviceLatestData
); */

// Get all devices from a fieldId
router.get('/field-devices/:field_id', deviceController.getDevicesByFieldId);

router.post('/', deviceController.createDevice);
// Factory routes
router.route('/').get(deviceController.getAllDevices);
// .post(deviceController.createDevice);

router
  .route('/:id')
  .get(deviceController.getDevice)
  .patch(deviceController.updateDevice)
  .delete(deviceController.deleteDevice);

/* router.delete(
  '/permanent-delete/:device_id',
  deviceController.deleteDevicePermanently
); */

module.exports = router;
