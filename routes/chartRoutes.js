const express = require("express");
const router = express.Router();
const chartController = require("./../controllers/chartController");

router
  .route("/")
  .get(chartController.getAllCharts)
  .post(chartController.createChart);

router
  .route("/:id")
  .get(chartController.getChart)
  .patch(chartController.updateChart)
  .delete(chartController.deleteChart);

module.exports = router;
