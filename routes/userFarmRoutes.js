// Importing
const express = require('express');
const router = express.Router();
const userFarmController = require('../controllers/userFarmController');

// Factory functions
router
  .route('/')
  .get(userFarmController.getAllUserFarms)
  .post(userFarmController.createUserFarm);

router
  .route('/:id')
  .get(userFarmController.getUserFarm)
  .patch(userFarmController.updateUserFarm)
  .delete(userFarmController.deleteUserFarm);

module.exports = router;
