// Importing
const express = require('express');
const router = express.Router();
const irrigationController = require('../controllers/irrigationController');

// Factory functions
router
  .route('/')
  .get(irrigationController.getAllPivots)
  .post(irrigationController.createPivot);

router
  .route('/:id')
  .get(irrigationController.getPivot)
  .patch(irrigationController.updatePivot)
  .delete(irrigationController.deletePivot);

module.exports = router;
