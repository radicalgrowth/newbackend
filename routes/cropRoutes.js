// Importing
const express = require('express');
const router = express.Router();
const cropController = require('../controllers/cropController');

// Factory functions
router
  .route('/')
  .get(cropController.getAllCrops)
  .post(cropController.createCrop);

router
  .route('/:id')
  .get(cropController.getCrop)
  .patch(cropController.updateCrop)
  .delete(cropController.deleteCrop);

module.exports = router;
