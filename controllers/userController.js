const User = require('./../models/User');
const UserFarm = require('./../models/UserFarm');
const Farm = require('./../models/Farm');
const Field = require('./../models/Field');
const Device = require('./../models/Device');
const Soil = require('./../models/Soil');
const Crop = require('./../models/Crop');
const Irrigation = require('./../models/Irrigation');
const Pivot = require('./../models/Pivot');
const Drip = require('./../models/Drip');
const SDI = require('./../models/SDI');
const Sprinkler = require('./../models/Sprinkler');
const Furrow = require('./../models/Furrow');

const factoryHandler = require('./factoryHandler');
const jwt = require('jsonwebtoken');
const Cryptr = require('cryptr');
const cryptr = new Cryptr(process.env.CRYPTR_SECRET);
const { body, check, validationResult } = require('express-validator');
// const validator = require('validator');
const multer = require('multer');
const { unlink } = require('fs/promises');
const S3 = require('./../utils/s3');
const sharp = require('sharp');

// comment for later use
/* const multerStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/img/users');
  },
  filename: (req, file, cb) => {
    const ext = file.mimetype.split('/')[1];
    cb(null, `user-${req.body.phone_number}-${Date.now()}.${ext}`);
  },
}); */
const multerStorage = multer.memoryStorage();

const multerFilter = (req, file, cb) => {
  if (file.mimetype.startsWith('image')) {
    cb(null, true);
  } else {
    cb(new Error('Not an image, Please upload only image file'), false);
  }
};

const upload = multer({ storage: multerStorage, fileFilter: multerFilter });

exports.uploadUserPhoto = upload.single('photo');

exports.resizeUserPhoto = async (req, res, next) => {
  if (!req.file) return next();

  req.file.filename = `user-${Date.now()}.jpeg`;
  req.file.path = `./public/img/users/${req.file.filename}`;

  try {
    await sharp(req.file.buffer)
      .flatten({ background: { r: 255, g: 255, b: 255 } })
      .resize(500, 500)
      .toFormat('jpeg')
      .jpeg({ quality: 90 })
      .toFile(`./public/img/users/${req.file.filename}`);
  } catch (err) {
    console.log(err);
  }

  next();
};

// Authentication functions
const signToken = (id) =>
  jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN,
  });

/* exports.validateRequest = (req, res, next) => {
  check('email').isEmail().withMessage('Please enter a valid email');
  check('password_confirm')
    .equals(req.body.password)
    .withMessage('Passwords do not match');

  next();
}; */

// Hash password middleware
exports.hashPassword = (req, res, next) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.status(400).json({ errors: errors.array() });
    }

    if (req.body.password) {
      const { password } = req.body;
      req.body.password = cryptr.encrypt(password);
    }
  } catch (err) {
    console.log(err);
    res.status(400).json({
      status: 'Error',
      message: err.message,
    });
  }
  next();
};

/* const createSendToken = (user) => {
  const token = signToken(user._id);

  return token;
  res.status(statusCode).json({
    status: 'success',
    token,
    data: {
      user,
    },
  }); 
}; */

exports.signup = async (req, res) => {
  await User.create(
    {
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
      role: req.body.role,
      gender: req.body.gender,
    },
    function (err, data) {
      if (err) {
        return res.status(400).json({
          status: 'Bad Request',
          message: err,
        });
      }
      res.status(200).json({
        status: 'Success',
        message: 'User successfully signed up!',
      });
    }
  );

  /* const user = User({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
    role: req.body.role,
    gender: req.body.gender,
  });

  await user.save(); */

  const url = `${req.protocol}://${req.get('host')}/me`;
  // console.log(url);
  //   await new Email(newUser, url).sendWelcome(); // For welcome email
};

exports.login = async (req, res) => {
  try {
    const { email, password } = req.body;

    // Check if email and password exists
    if (!email || !password) {
      return res.status(500).json({
        status: 'Bad Request',
        message: `${!email ? 'Please enter email' : 'Please enter password'}`,
      });
    }

    // Check if user exists
    const user = await User.findOne({ email, archive: false }).select(
      '+password'
    );

    if (!user) {
      return res.status(400).json({
        status: 'Bad Request',
        message: 'User not found',
      });
    } else if (!(await user.comparePassword(password, user.password))) {
      return res.status(400).json({
        status: 'Bad Request',
        message: 'Password wrong. Please enter correct password',
      });
    }

    // If everything ok, then create jwt token
    const token = signToken(user._id);
    // console.log({ token, user });

    // Append token in user's token array
    // user.appendToken(token);
    user.tokens.push({ token });
    await user.save();

    // Send token to client
    res.status(200).json({
      status: 'Success',
      token,
      body: user,
    });
  } catch (err) {
    console.log(err);
    res.status(400).json({
      status: 'Error',
      message: err,
    });
  }
};

exports.forgotPassword = async (req, res) => {
  const { email } = req.body;
  const user = await User.findOne({ email, archive: false }).select('password');
  const password = cryptr.decrypt(user.password);

  res.status(200).json({
    status: 'Success',
    body: password,
  });
};

// Upload file
exports.createUser = async (req, res, next) => {
  try {
    // console.log({ body: req, file: req.file });
    let data = new User({
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
      role: req.body.role,
      phone_number: req.body.phone_number,
      phone_code: req.body.phone_code,
      city: req.body.city,
      country: req.body.country,
      organization: req.body.organization,
      gender: req.body.gender,
      application_name: req.body.application_name,
      user_id: req.body.user_id ? req.body.user_id : null,
    });
    if (req.file) {
      // Upload image to S3
      const s3Result = await S3.uploadFile(req.file);
      (data.photo_name = req.file.filename), (data.photo = s3Result.Location);
    } else {
      // edit it later, get default image from s3 bucket
      // data.photo = 'default.png';
      data.photo =
        'https://new-backend-file-storage-bucket.s3.us-east-2.amazonaws.com/default.jpg';
    }

    const newUser = await data.save();

    res.status(200).json({
      status: 'Success',
      message: 'User created',
      body: newUser,
    });
  } catch (err) {
    console.log(err);
    res.status(400).json({
      status: 'Error',
      message: err,
    });
  }
};

// Update profile
exports.updateUser = async (req, res, next) => {
  try {
    const { id } = req.params;
    const user = await User.findById(id).select('-application_name -tokens');

    if (!user || user.length == 0) {
      return res.status(400).json({
        status: 'Bad Request',
        message: 'User not found',
      });
    }
    const data = {
      name: req.body.name,
      email: req.body.email,
      role: req.body.role,
      phone_number: req.body.phone_number,
      city: req.body.city,
      country: req.body.country,
      organization: req.body.organization,
      gender: req.body.gender,
    };

    if (req.body.password) data.password = cryptr.encrypt(req.body.password);

    if (req.file) {
      let newImage = await S3.uploadFile(req.file);
      // Check if image already exist
      // This code not working since I didn't convert buffer to string
      /* const checkImage = await S3.getFile(user.photo_name);
      if (checkImage) {
        // Delete image from S3
        // await S3.deleteFile(user.photo_name);
      } else {
        // upload new request image to S3
      } */

      // allocating new image to user
      data.photo_name = req.file.filename;
      data.photo = newImage.Location;
    }

    const updatedUser = await User.findByIdAndUpdate(id, data, {
      new: true,
      runValidators: true,
    });

    // Use this for later
    // await unlink(`./public/img/users/${user.photo}`);

    res.status(200).json({
      status: 'Success',
      message: 'User updated',
      body: updatedUser,
    });
  } catch (err) {
    console.log(err);
    res.status(400).json({
      status: 'Error',
      message: err,
    });
  }
};

// Delete user and delete his image from public/img/users
exports.deleteUser = async (req, res) => {
  try {
    const user = await User.findById(req.params.user_id).select('-tokens');
    // Delete image from S3
    if (user.photo_name !== 'default.jpg') {
      await S3.deleteFile(user.photo_name);
    }
    const userFarms = await UserFarm.find({ user_id: user._id });
    for (let a = 0; a < userFarms.length; a++) {
      // Delete all farms of user
      await Farm.findByIdAndDelete(userFarms[a].farm_id);

      // Get field data
      const field = await Field.find({ farm_id: userFarms[a].farm_id });

      for (let i = 0; i < field.length; i++) {
        // Delete any devices associated with a field
        const devices = await Device.find({ field_id: field[i]._id });
        if (devices) {
          await Device.deleteMany({ field_id: field[i]._id });
        }
        // Delete any Soil associated with field
        await Soil.findOneAndDelete({ field_id: field[i]._id });

        // Delete any crop associate with field
        await Crop.findOneAndDelete({ field_id: field[i]._id });

        // Get irrigation associate with field
        const irrigation = await Irrigation.findOne({
          field_id: field[i]._id,
        }).select('_id');
        // Delete any system type associated with irrigation
        if (irrigation.system_type === 'pivot') {
          await Pivot.findOneAndDelete({ irrigation_id: irrigation._id });
        } else if (irrigation.system_type === 'drip') {
          await Drip.findOneAndDelete({ irrigation_id: irrigation._id });
        } else if (irrigation.system_type === 'sdi') {
          await SDI.findOneAndDelete({ irrigation_id: irrigation._id });
        } else if (irrigation.system_type === 'sprinkler') {
          await Sprinkler.findOneAndDelete({ irrigation_id: irrigation._id });
        } else if (irrigation.system_type === 'furrow') {
          await Furrow.findOneAndDelete({ irrigation_id: irrigation._id });
        }
        // Delete any irrigation associated with field
        await Irrigation.findOneAndDelete({ field_id: field[i]._id });
        // Delete field
        await Field.findOneAndDelete({ _id: field[i]._id });
      }
    }
    if (userFarms.length > 1) {
      await UserFarm.deleteMany({ user_id: user._id });
    } else {
      await UserFarm.findOneAndDelete({ user_id: user._id });
    }

    // Delete user from DB
    await User.findByIdAndDelete(req.params.user_id);

    res.status(200).json({
      status: 'Success',
      message: 'User deleted permanently',
    });
  } catch (err) {
    console.log(err);
    res.status(400).json({
      status: 'Error',
      message: err.message,
    });
  }
};

// Factory Functions
exports.getAllUsers = async (req, res) => {
  try {
    /* let { page, size } = req.query;
    if (!page) page = 1;
    if (!size) size = 2;

    const limit = parseInt(size);
    const skip = (page - 1) * limit; */

    // let query = Model.find({}).limit(limit).skip(skip);
    let query = await User.find({ archive: false })
      .sort({ timestamp: -1 })
      .select('-tokens');

    let docs = await query;

    if (!query) {
      return res.status(404).json({
        status: 'Not Found',
        message: 'No document found with that ID',
      });
    }

    res.status(200).json({
      status: 'Success',
      result: docs.length,
      body: {
        total_elements: docs.length,
        items: docs,
      },
    });
  } catch (err) {
    res.status(400).json({
      status: 'Bad Request',
      message: err.message,
    });
  }
};

exports.deleteUserPermanently = async (req, res) => {
  try {
    const user = await User.findById(req.params.user_id);
    // Delete image from S3
    if (user.photo_name !== 'default.jpg') {
      await S3.deleteFile(user.photo_name);
    }
    // Delete user from DB
    await User.findByIdAndDelete(req.params.user_id);

    res.status(200).json({
      status: 'Success',
      message: 'User deleted permanently',
    });
  } catch (err) {
    console.log(err);
    res.status(400).json({
      status: 'Bad Request',
      message: err.message,
    });
  }
};

// exports.getUser = factoryHandler.getOne(User);
exports.getUser = async (req, res) => {
  try {
    const query = User.findById(req.params.id).select('-tokens -__v -password');

    if (query.user_id != '') {
      query.populate('user_id', 'name');
    }
    const user = await query.lean();

    if (!user) {
      return res.status(400).json({
        status: 'Bad Request',
        message: 'User not found',
      });
    }
    res.status(200).json({
      status: 'Success',
      body: user,
    });
  } catch (err) {
    console.log(err);
    res.status(400).json({
      status: 'Bad Request',
      message: err.message,
    });
  }
};
// factoryHandler.getAllDocs(User);

exports.getUserWorkers = async (req, res) => {
  try {
    const user = await User.findById(req.params.user_id).select('role');
    // console.log({ user });
    let query;
    if (user.role === 'super_admin') {
      query = User.find({
        archive: false,
        _id: { $ne: req.params.user_id },
      }).lean();
    } else if (user.role === 'admin') {
      query = User.find({
        user_id: req.params.user_id,
        archive: false,
        role: 'worker',
      }).lean();
    }

    let users = await query;

    if (!users || users.length === 0) {
      return res.status(200).json({
        status: 'Bad Request',
        message: 'No workers found under this user',
        body: {
          total_elements: users.length,
          items: users,
        },
      });
    }

    res.status(200).json({
      status: 'Success',
      body: {
        total_elements: users.length,
        items: users,
      },
    });
  } catch (err) {
    console.log(err);
    res.status(400).json({
      status: 'Bad Request',
      message: err.message,
    });
  }
};

//////////////////////////////////////////
/* ARCHIVE

// Add this for later
    const user = await User.findById(req.params.id);
    // Make user inactive
    user.active = false;
    await user.save();

*/
