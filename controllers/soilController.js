const Soil = require('../models/Soil');
const factoryHandler = require('./factoryHandler');

exports.createSoil = factoryHandler.createOne(Soil);
exports.getAllSoils = factoryHandler.getAllDocs(Soil);
exports.getSoil = factoryHandler.getOne(Soil);
exports.updateSoil = factoryHandler.updateOne(Soil);
exports.deleteSoil = factoryHandler.deleteOne(Soil);
