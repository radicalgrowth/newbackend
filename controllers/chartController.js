const Chart = require("./../models/Chart");
const factoryHandler = require("./factoryHandler");

exports.createChart = factoryHandler.createOne(Chart);
exports.getAllCharts = factoryHandler.getAllDocs(Chart);
exports.getChart = factoryHandler.getOne(Chart);
exports.updateChart = factoryHandler.updateOne(Chart);
exports.deleteChart = factoryHandler.deleteOne(Chart);
