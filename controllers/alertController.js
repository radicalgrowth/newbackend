const Alert = require('./../models/Alert');
const factoryHandler = require('./factoryHandler');

exports.getAlertsByUserId = async (req, res) => {
  try {
    const alerts = await Alert.find({
      user_id: req.params.user_id,
      archive: false,
    })
      .populate('device_id', 'name')
      .lean();
    for (let i = 0; i < alerts.length; i++) {
      // console.log({ device: alerts[i].device_id });
      alerts[i].device_name = alerts[i].device_id.name;
      delete alerts[i].device_id;
    }
    if (!alerts) {
      return res.status(404).json({
        status: 'Error',
        message: 'No alerts found',
      });
    }

    res.status(200).json({
      status: 'Success',
      body: {
        total_elements: alerts.length,
        items: alerts,
      },
    });
  } catch (err) {
    console.log(err);
    return res.status(400).json({
      status: 'Error',
      message: err.message,
    });
  }
};

exports.getUserUsedAlerts = async (req, res) => {
  try {
    const alerts = await Alert.find({
      user_id: req.params.user_id,
      archive: false,
      sent: { $gte: 1 },
    })
      .populate('device_id', 'name')
      .lean();

    for (let i = 0; i < alerts.length; i++) {
      // console.log({ device: alerts[i].device_id });
      alerts[i].device_name = alerts[i].device_id.name;
      delete alerts[i].device_id;
    }
    if (!alerts) {
      return res.status(404).json({
        status: 'Error',
        message: 'No alerts found',
      });
    }

    res.status(200).json({
      status: 'Success',
      body: {
        total_elements: alerts.length,
        items: alerts,
      },
    });
  } catch (err) {
    console.log(err);
    return res.status(400).json({
      status: 'Error',
      message: err.message,
    });
  }
};

exports.createAlert = factoryHandler.createOne(Alert);
exports.getAllAlerts = factoryHandler.getAllDocs(Alert);
exports.getAlert = factoryHandler.getOne(Alert);
exports.updateAlert = factoryHandler.updateOne(Alert);
exports.deleteAlert = factoryHandler.deleteOne(Alert);

exports.deletePermanently = async (req, res) => {
  try {
    const alert = await Alert.findByIdAndDelete(req.params.id);
    if (!alert) {
      return res.status(400).json({
        status: 'Error',
        message: 'Alert not found',
      });
    }

    res.status(200).json({
      status: 'Success',
      body: 'Alert deleted permanently',
    });
  } catch (err) {
    console.log(err);
    return res.status(400).json({
      status: 'Error',
      message: err.message,
    });
  }
};
