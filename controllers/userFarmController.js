const Farm = require('../models/Farm');
const UserFarm = require('../models/UserFarm');
const factoryHandler = require('./factoryHandler');

exports.createUserFarm = async (req, res) => {
  const userFarm = await UserFarm.create({
    user_id: req.body.user_id,
    farm_id: req.body.farm_id,
  });
  //   console.log({ userFarmData: userFarm });

  return res.status(200).json({
    status: 'Success',
    data: userFarm,
  });
};

exports.getAllUserFarms = factoryHandler.getAllDocs(UserFarm);
exports.getUserFarm = factoryHandler.getOne(UserFarm, ['farm_id', 'user_id']);
exports.updateUserFarm = factoryHandler.updateOne(UserFarm);
exports.deleteUserFarm = async (req, res) => {
  const userFarm = await UserFarm.findById(req.params.id);

  await Farm.findOneAndDelete({ _id: userFarm.farm_id });
  await UserFarm.findByIdAndDelete(req.params.id);

  return res.status(200).json({
    status: 'Success',
    message: 'Document deleted Successfully',
  });
};
