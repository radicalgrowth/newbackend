const Device = require('./../models/Device');

exports.createOne = (Model) => async (req, res, next) => {
  try {
    const doc = await Model.create(req.body);

    res.status(200).json({
      status: 'Success',
      body: doc,
    });
  } catch (err) {
    console.log(err);
    res.status(400).json({
      status: 'Bad Request',
      message: err.message,
    });
  }
};

exports.getAllDocs = (Model, popOptions) => async (req, res) => {
  try {
    /* let { page, size } = req.query;
    if (!page) page = 1;
    if (!size) size = 2;

    const limit = parseInt(size);
    const skip = (page - 1) * limit; */

    // let query = Model.find({}).limit(limit).skip(skip);
    let query = Model.find({ archive: false }).sort({ timestamp: -1 });

    if (popOptions) {
      // Separate model from popOptions
      let model = popOptions[0];
      // Delete populated field from popOptions
      delete popOptions[0];
      // Populate the query
      query = query.populate(model, popOptions);
    }

    let docs = await query;

    if (!query) {
      return res.status(404).json({
        status: 'Not Found',
        message: 'No document found with that ID',
      });
    }

    res.status(200).json({
      status: 'Success',
      result: docs.length,
      body: {
        total_elements: docs.length,
        items: docs,
      },
    });
  } catch (err) {
    res.status(400).json({
      status: 'Bad Request',
      message: err.message,
    });
  }
};

exports.getOne = (Model, popOptions) => async (req, res, next) => {
  try {
    let query = Model.findById(req.params.id);

    if (popOptions) {
      // Separate model from popOptions
      let model = popOptions[0];
      // Delete populated field from popOptions
      delete popOptions[0];
      // Populate the query
      query = query.populate(model, popOptions);
    }

    const doc = await query;

    if (!doc) {
      return res.status(404).json({
        status: 'Not Found',
        message: 'No document found with that ID',
      });
    }

    res.status(200).json({
      status: 'Success',
      body: doc,
    });
  } catch (err) {
    res.status(400).json({
      status: 'Bad Request',
      message: err.message,
    });
  }
};

exports.updateOne = (Model) => async (req, res, next) => {
  try {
    const doc = await Model.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    });

    if (!doc || doc === null) {
      return res.status(404).json({
        status: 'Not Found',
        message: 'No document found with that ID',
      });
    }

    return res.status(200).json({
      status: 'Success',
      body: doc,
    });
  } catch (err) {
    res.status(400).json({
      status: 'Bad Request',
      message: err.message,
    });
  }
};

exports.deleteOne = (Model) => async (req, res, next) => {
  try {
    const doc = await Model.findByIdAndDelete(req.params.id);
    /* const doc = await Model.findById(req.params.id).select('_id');
    doc.archive = true;
    await doc.save(); */

    if (!doc) {
      return res.status(404).json({
        status: 'Not Found',
        message: 'No document found with that ID',
      });
    }

    res.status(200).json({
      status: 'Success',
      message: `${Model} deleted successfully`,
    });
  } catch (err) {
    res.status(400).json({
      status: 'Bad Request',
      message: err.message,
    });
  }
};
