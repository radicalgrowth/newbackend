const Influx = require('influx');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const Device = require('./../models/Device');
const User = require('./../models/User');
const UserFarms = require('./../models/UserFarm');
const Field = require('./../models/Field');
const Alert = require('./../models/Alert');
const factoryHandler = require('./factoryHandler');
const moment = require('moment');

// Influx DB connection
var influx = new Influx.InfluxDB({
  host: process.env.INFLUX_CON_STRING,
  username: process.env.INFLUX_USERNAME,
  password: process.env.INFLUX_PASSWORD,
  port: process.env.INFLUX_PORT,
  database: process.env.INFLUX_DB,
});

// Factory functions
exports.updateDevice = factoryHandler.updateOne(Device);
exports.deleteDevice = async (req, res) => {
  try {
    // delete any alert associated with it.
    const alerts = await Alert.find({ device_id: req.params.id });
    if (alerts.length > 0) {
      for (let i = 0; i < alerts.length; i++) {
        await Alert.findByIdAndDelete(alerts[i]._id);
      }
    }

    // const device = await Device.findById(req.params.id);
    // console.log({ alerts, device });
    await Device.findByIdAndDelete(req.params.id);

    res.status(200).json({
      status: 'Success',
      message: 'Device deleted successfully',
    });
  } catch (err) {
    console.log(err);
    res.status(400).json({
      message: err.message,
    });
  }
};

exports.deleteDevicePermanently = async (req, res) => {
  try {
    const device = await Device.findOneAndDelete({ _id: req.params.device_id });
    if (!device) {
      return res.status(404).json({
        status: 'Not Found',
        message: 'No device found with that ID',
      });
    }

    return res.status(200).json({
      status: 'Success',
      message: 'Device deleted permanently Successfully',
    });
  } catch (err) {
    console.log(err);
  }
};

exports.createDevice = async (req, res) => {
  try {
    // let device = await Device.findOne({ device_topic: req.body.device_topic });
    let device = await Device.findOne({ name: req.body.name });
    /* if (device) {
      return res.status(400).json({
        status: 'Bad Request',
        message: 'Device already exists',
      });
    } */

    device = new Device({
      name: req.body.name,
      fields: req.body.fields,
      device_type: req.body.device_type,
      field_id: req.body.field_id,
      application_name: req.body.application_name,
      device_topic: req.body.device_topic,
      device_location: req.body.device_location,
    });

    await influx
      .query(
        `SELECT last(uplink_message_decoded_payload_battery) FROM mqtt_consumer WHERE topic = '${device.device_topic}'`
      )
      .then((result) => {
        for (let i = 0; i < result.length; i++) {
          if (result[i].last > 0) {
            device.status = 'active';
          }
        }
      });

    await device.save();

    res.status(201).json({
      status: 'Success',
      body: device,
    });
  } catch (err) {
    console.log(err);
    res.status(400).json({
      error: err,
    });
  }
};

// exports.getDevice = factoryHandler.getOne(Device, ['user_id', 'name']);
exports.getDevice = async (req, res) => {
  try {
    let device = await Device.findOne({ _id: req.params.id, archive: false })
      .select('-__v -timestamp')
      .populate('field_id', 'name')
      .lean();

    if (!device) {
      return res.status(404).json({
        status: 'Not Found',
        message: 'No document found with that ID',
      });
    }

    for (let j = 0; j < device.fields.length; j++) {
      if (device.fields[j] !== null && device.fields[j] !== '') {
        await influx
          .query(
            `select last(${device.fields[j]}) from mqtt_consumer where topic = '${device.device_topic}'`
          )
          .then((results) => {
            // console.log({ influxResults: results[0] });
            if (results.length > 0) {
              device.fields[j] = {
                name: device.fields[j],
                last: results[0].last,
              };
            } else {
              device.fields[j] = {
                name: device.fields[j],
                last: 0,
              };
            }
          });
      }
    }

    const airDevice = await Device.findOne({
      device_type: 'air',
      field_id: device.field_id._id,
    })
      .select('name device_topic')
      .lean();

    if (airDevice) {
      // Get the last value of airtemp and humidity
      const getAirDeviceReading = await influx.query(
        `SELECT last(uplink_message_decoded_payload_airtemp) AS airtemp, last(uplink_message_decoded_payload_humidity) AS humidity FROM mqtt_consumer WHERE topic='${airDevice.device_topic}'`
      );
      device.airDevice = getAirDeviceReading[0];
    }
    // console.log({ getAirDeviceReading });

    res.status(200).json({
      status: 'Success',
      body: device,
    });
  } catch (err) {
    console.log(err);
    res.status(400).json({
      error: err,
    });
  }
};

// exports.getAllDevices = factoryHandler.getAllDocs(Device, ['user_id', 'name']);
exports.getAllDevices = async (req, res) => {
  try {
    // For pagination in future
    /* let { page, size } = req.query;
    if (!page) page = 1;
    if (!size) size = 10;

    const limit = parseInt(size);
    const skip = (page - 1) * size; */

    /* let devices = await Device.find({})
      .limit(limit)
      .skip(skip)
      .sort({
        timestamp: 'desc',
      })
      .populate('user_id', ['name'])
      .lean(); */

    let devices = await Device.find({ archive: false })
      .sort({ timestamp: -1 })
      // .populate('user_id', ['name'])
      .lean();

    // Let us get the last value of each field
    /* for (let i = 0; i < devices.length; i++) {
      for (let j = 0; j < devices[i].fields.length; j++) {
        if (devices[i].fields[j] !== null && devices[i].fields[j] !== '') {
          await influx
            .query(
              `select last(${devices[i].fields[j]}) from mqtt_consumer where topic = '${devices[i].device_topic}'`
            )
            .then((results) => {
              // console.log({ influxResults: results[0] });
              if (results.length > 0) {
                devices[i].fields[j] = {
                  name: devices[i].fields[j],
                  last: results[0].last,
                };
              } else {
                devices[i].fields[j] = {
                  name: devices[i].fields[j],
                  last: 0,
                };
              }
            });
        }
      }
    } */

    // Return the results
    res.status(200).json({
      status: 'Success',
      body: {
        total_elements: devices.length,
        items: devices,
      },
    });
  } catch (err) {
    console.log(err);
    res.status(400).json({
      error: err,
    });
  }
};

exports.getThingStackDevices = async (req, res) => {
  try {
    // Implement pagination
    /* let { page, size } = req.query;
    if (!page) page = 1;
    if (!size) size = 10;

    const limit = parseInt(size);
    const skip = (page - 1) * size; */

    let data = [];
    // `SHOW TAG VALUES ON "telegraf" WITH KEY = "topic" limit ${limit} offset ${skip}`
    await influx
      .query(`SHOW TAG VALUES ON "telegraf" WITH KEY = "topic"`)
      .then((results) => {
        results.forEach((element) => {
          if (
            element.value.includes('deployment-001/admin@gmail.com/devices/')
          ) {
            let value = element.value.replace(
              'deployment-001/admin@gmail.com/devices/',
              ''
            );
            let payload = {
              label: value,
              application_name: element.value.split('/')[0],
              value: element.value,
            };
            data.push(payload);
          } else if (
            element.value.includes(
              'deployment-001/superadmin@radicalgrowth.solutions/devices/'
            )
          ) {
            let value = element.value.replace(
              'deployment-001/superadmin@radicalgrowth.solutions/devices/',
              ''
            );
            let payload = {
              label: value,
              application_name: element.value.split('/')[0],
              value: element.value,
            };
            data.push(payload);
          } else if (
            element.value.includes(
              'deployment-001/ali.shahbaz@telenor.com.pk/devices/'
            )
          ) {
            let value = element.value.replace(
              'deployment-001/ali.shahbaz@telenor.com.pk/devices/',
              ''
            );
            let payload = {
              label: value,
              application_name: element.value.split('/')[0],
              value: element.value,
            };
            data.push(payload);
          }
          /* if (element.value.includes('v3/rgs-test/devices/')) {
            let value = element.value.replace('v3/rgs-test/devices/', '');
            let payload = {
              label: value,
              application_name: element.value.split('/')[1],
              value: element.value,
            };
            data.push(payload);
          }  else if (
            element.value.includes(
              'deployment-001/admin/Admin Farm/Admin Field 1/devices/'
            )
          ) {
            let value = element.value.replace(
              'deployment-001/admin/Admin Farm/Admin Field 1/devices/',
              ''
            );
            let payload = {
              label: value,
              application_name: element.value.split('/')[0],
              value: element.value,
            };
            data.push(payload);
          } */
        });
      });

    res.status(200).json({
      status: 'Success',
      body: {
        total_elements: data.length,
        items: data,
      },
    });
  } catch (err) {
    console.log(err);
    res.status(400).json({
      error: err,
    });
  }
};

exports.getThingStackFields = async (req, res) => {
  try {
    // Implement pagination
    // prefer dont use pagination for this request
    /* let { page, size } = req.query;
    if (!page) page = 1;
    if (!size) size = 10;

    const limit = parseInt(size);
    const skip = (page - 1) * size; */

    let data = [];
    await influx
      .query(`SHOW FIELD KEYS ON "telegraf" FROM "mqtt_consumer"`)
      .then((results) => {
        // results.splice(2, 4);
        results.forEach((element) => {
          let value, payload;
          if (
            element.fieldKey.includes('uplink_message_decoded_payload_') &&
            element.fieldType === 'float'
          ) {
            value = element.fieldKey.replace(
              'uplink_message_decoded_payload_',
              ''
            );
            // console.log({ element });
            if (
              value != 'converted_long' &&
              value != 'converted_gndmoist' &&
              value != 'converted_gndmoist1' &&
              value != 'converted_gndmoist2' &&
              value != 'converted_lat' &&
              value != 'converted_long' &&
              value != 'lat' &&
              value != 'long' &&
              value != 'num' &&
              value != 'temp' &&
              value != 'gndmoist' &&
              value != 'gndtemp'
            ) {
              payload = {
                label: value,
                value: element.fieldKey,
              };
              data.push(payload);
            }
          }
        });
      });

    res.status(200).json({
      status: 'Success',
      body: {
        total_elements: data.length,
        items: data,
      },
    });
  } catch (err) {
    console.log(err);
  }
};

exports.getDevicesByUserId = async (req, res) => {
  try {
    // Implement pagination
    /* let { page, size } = req.query;
    if (!page) page = 1;
    if (!size) size = 10;

    const limit = parseInt(size);
    const skip = (page - 1) * size; */

    // Get all farms of a user
    let deviceData = [];
    const farmsData = await UserFarms.find({
      user_id: req.params.user_id,
      archive: false,
    })
      .select('farm_id')
      .lean();
    if (!farmsData) {
      return res.status(400).json({
        status: 'Error',
        message: 'User has no farms. Please create a farm',
      });
    }

    // Get all fields of farms
    for (let i = 0; i < farmsData.length; i++) {
      const fields = await Field.find({
        farm_id: farmsData[i].farm_id,
        archive: false,
      })
        .select('_id')
        .lean();
      if (!fields) {
        return res.status(400).json({
          status: 'Error',
          message: 'Farm has no fields. Please create a field',
        });
      }
      for (let j = 0; j < fields.length; j++) {
        const devices = await Device.find({
          field_id: fields[j]._id,
          archive: false,
        }).lean();
        if (!devices) {
          return res.status(400).json({
            status: 'Error',
            message: 'Field has no devices',
          });
        }

        // insert all devices to deviceData
        deviceData.push(...devices);
      }
    }
    res.status(200).json({
      status: 'Success',
      body: {
        // total_elements: devices.length,
        items: deviceData,
      },
    });
  } catch (err) {
    console.log(err);
    res.status(400).json({
      error: err,
    });
  }
};

exports.deleteDeviceByUserId = async (req, res) => {
  try {
    await Device.findOneAndDelete({ user_id: req.params.userid });
    return res.status(200).json({
      status: 'Success',
      message: 'Device Deleted Successfully',
    });
  } catch (err) {
    console.log(err);
  }
};

exports.getDeviceDataById = async (req, res) => {
  // let { id, field, time } = req.body;
  let { device_topic, field, from, to } = req.body;
  let Mydata = [];
  // My code
  // select mean(uplink_message_decoded_payload_battery) from telegraf.autogen.mqtt_consumer where time > now() - '2021-06-23T13:32:46.000Z' AND topic='v3/rgs-test/devices/rgs-test-02/up' GROUP BY time(5m)

  if (field.includes('uplink_message_decoded_payload_gndmoist1')) {
    field = field.replace(
      'uplink_message_decoded_payload_gndmoist1',
      'uplink_message_decoded_payload_converted_gndmoist1 AS uplink_message_decoded_payload_gndmoist'
    );
  }

  if (field.includes('uplink_message_decoded_payload_gndmoist2')) {
    field = field.replace(
      'uplink_message_decoded_payload_gndmoist2',
      'uplink_message_decoded_payload_converted_gndmoist2 AS uplink_message_decoded_payload_gndmoist2'
    );
  }

  // Old query
  /* Mydata = await influx.query(
    `select ${field} from mqtt_consumer where time >= '${time}' AND topic = '${deviceid}' GROUP BY time(5m)`
  ); */

  // For testing
  Mydata = await influx.query(
    `select ${field} from mqtt_consumer where time >= '${from}' AND time <= '${to}' AND topic = '${device_topic}'`
  );

  for (let i = 0; i < Mydata.length; i++) {
    // add 5 hours to Mydata timestamp
    Mydata[i].time = moment(Mydata[i].time).add(5, 'hours');
  }

  return res.status(200).json({
    status: 'Success',
    body: {
      total_elements: Mydata.length,
      items: Mydata,
    },
  });
};

exports.getMultipleDeviceData = async (req, res) => {
  const { devices, fields, time } = req.body;

  // Refactor the fields to be fetched
  if (fields.includes('uplink_message_decoded_payload_gndmoist')) {
    fields = fields.replace(
      'uplink_message_decoded_payload_gndmoist',
      'uplink_message_decoded_payload_converted_gndmoist AS uplink_message_decoded_payload_gndmoist'
    );
  }

  if (fields.includes('uplink_message_decoded_payload_gndmoist2')) {
    fields = fields.replace(
      'uplink_message_decoded_payload_gndmoist2',
      'uplink_message_decoded_payload_converted_gndmoist2 AS uplink_message_decoded_payload_gndmoist2'
    );
  }

  // Refactor the devices
  let totalDevices = devices.split(',');
  let totalDeviceData = [];

  // Loop through the devices
  for (let i = 0; i < totalDevices.length; i++) {
    const data = await influx.query(
      `SELECT ${fields} FROM mqtt_consumer WHERE topic = '${totalDevices[i]}' AND time > '${time}'`
    );
    let payload = {
      name: totalDevices[i],
      data: data,
    };
    totalDeviceData.push(payload);
  }

  res.status(200).json({
    status: 'Success',
    body: {
      total_elements: totalDeviceData.length,
      items: totalDeviceData,
    },
  });
};

exports.getAllDevicesData = async (req, res) => {
  // Get all request body from frontend
  const { userId, field } = req.body;

  // Get all devices by userId
  const devices = await Device.find({ user_id: userId, archive: false });

  // Get all common deviceParameters in all devices
  let commonDeviceParameters = [];
  for (let i = 0; i < devices.length; i++) {
    if (devices[i].fields.includes(field)) {
      commonDeviceParameters.push({
        device: devices[i].device_topic,
        field: field,
      });
    }
  }

  // Make a query for each device
  for (let i = 0; i < commonDeviceParameters.length; i++) {
    let data = await influx.query(
      `SELECT last(${commonDeviceParameters[i].field}) FROM mqtt_consumer WHERE topic = '${commonDeviceParameters[i].device}'`
    );
    commonDeviceParameters[i].data = data[0];
  }

  res.status(200).json({
    status: 'Success',
    body: {
      total_elements: commonDeviceParameters.length,
      items: commonDeviceParameters,
    },
  });
};

// This request gives us all parameters last data
exports.getDeviceParametersById = async (req, res) => {
  try {
    // My Code
    const device = await Device.findById(req.params.id);
    // My Code
    if (!device) {
      return res.status(404).json({
        status: 'Failed',
        message: 'Device not found',
      });
    }

    Mydata = null;
    await influx
      .query(`SHOW FIELD KEYS ON telegraf FROM mqtt_consumer`)
      .then((results) => {
        Mydata = results;
      });
    var filtered_feilds = await Mydata.filter(function (val) {
      // return val.fieldKey.startsWith(deviceid);
      return val.fieldKey;
    });

    for (let i = 0; i < filtered_feilds.length; i++) {
      if (
        filtered_feilds[i].fieldKey !== null &&
        filtered_feilds[i].fieldKey !== ''
      ) {
        await influx
          .query(
            `select last(${filtered_feilds[i].fieldKey}) from mqtt_consumer where topic ='${device.device_topic}'`
          )
          .then((results) => {
            if (results.length > 0) {
              filtered_feilds[i]['last'] = results[0].last;
            }
          });
      }
    }
    /* console.log({
      message: "data after last query",
      result: filtered_feilds,
    }); */
    return res.status(200).json({
      status: 'Success',
      body: {
        total_elements: filtered_feilds.length,
        items: filtered_feilds,
      },
    });
  } catch (error) {
    console.log(error);
    res.status(400).json({
      status: 'Error',
      message: error,
    });
  }
};

exports.checkDeviceAlreadyRegistered = async (req, res) => {
  try {
    const device = await Device.findOne({
      device_topic: req.body.device_topic,
      archive: false,
    });
    if (!device) {
      return res.status(200).json({
        status: 'Success',
        message: 'Device not registered',
      });
    } else {
      return res.status(200).json({
        status: 'Success',
        message: 'Device registered',
      });
    }
  } catch (error) {
    res.status(400).json({
      status: 'Error',
      message: error,
    });
  }
};

exports.getDeviceLastDataById = async (req, res) => {
  try {
    Mydata = null;
    await influx
      .query(
        `select last(${req.body.field}) from mqtt_consumer where topic ='${req.body.id}' ORDER BY ASC LIMIT 1`
      )
      .then((results) => {
        Mydata = results;
      });
    return res.status(200).json({
      status: 'Success',
      body: {
        total_elements: Mydata.length,
        items: Mydata,
      },
    });
  } catch (error) {
    console.log(error);
    res.status(400).json({
      status: 'Error',
      message: error,
    });
  }
};

exports.getDevicesByFieldId = async (req, res) => {
  try {
    const devices = await Device.find({
      field_id: req.params.field_id,
      archive: false,
    })
      .sort('-timestamp -__v')
      .select('-__v -timestamp')
      .lean();

    if (!devices) {
      return res.status(400).json({
        status: 'Error',
        message: 'Devices not found',
      });
    }

    for (let i = 0; i < devices.length; i++) {
      const fieldName = await Field.findById(devices[i].field_id).select(
        'name'
      );
      devices[i].field_name = fieldName.name;
      await influx
        .query(
          `SELECT last(uplink_message_decoded_payload_converted_gndmoist1) as moisture FROM mqtt_consumer WHERE topic = '${devices[i].device_topic}'`
        )
        .then((result) => {
          if (result) {
            const currentTime = new Date();

            const calculatedMinutes =
              (currentTime - result[0].time) / (1000 * 60); // calculated time in minutes

            if (calculatedMinutes < 18) devices[i].status = 'active';
            else if (calculatedMinutes > 18) devices[i].status = 'inactive';

            let moisture = result[0].moisture;
            devices[i].moisture = moisture;
            if (moisture <= 40) {
              devices[i].moisture_status = 'low';
            } else if (moisture >= 40) {
              devices[i].moisture_status = 'high';
            }
          }
        });
    }

    return res.status(200).json({
      status: 'Success',
      body: {
        total_elements: devices.length,
        items: devices,
      },
    });
  } catch (err) {
    console.log(err);
    return res.status(400).json({
      status: 'Error',
      message: err,
    });
  }
};

/* exports.getDeviceLatestData = async (req, res) => {
  try {
    const device = await Device.findById(req.params.device_topic)
      .select('fields device_topic')
      .lean();

    for (let i = 0; i < device.fields.length; i++) {
      await influx
        .query(
          `select last(${device.fields[i]}) from mqtt_consumer where topic ='${device.device_topic}'`
        )
        .then((result) => {
          if (result.length > 0) {
            device.fields[i] = {
              name: device.fields[i],
              last: result[0].last,
            };
          }
        });
    }
    delete device.device_topic;
    // console.log({ device });

    return res.status(200).json({
      status: 'Success',
      body: device,
    });
  } catch (err) {
    console.log(err);
    return res.status(400).json({
      status: 'Error',
      message: err,
    });
  }
}; */
