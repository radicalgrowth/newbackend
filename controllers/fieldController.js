const Influx = require('influx');
const Farm = require('../models/Farm');
const UserFarm = require('../models/UserFarm');
const Device = require('../models/Device');
const Field = require('../models/Field');
const Soil = require('../models/Soil');
const Crop = require('../models/Crop');
const Irrigation = require('../models/Irrigation');
const Pivot = require('../models/Pivot');
const Drip = require('../models/Drip');
const SDI = require('../models/SDI');
const Sprinkler = require('../models/Sprinkler');
const Furrow = require('../models/Furrow');
const factoryHandler = require('./factoryHandler');

// Influx DB connection
var influx = new Influx.InfluxDB({
  host: process.env.INFLUX_CON_STRING,
  username: process.env.INFLUX_USERNAME,
  password: process.env.INFLUX_PASSWORD,
  port: process.env.INFLUX_PORT,
  database: process.env.INFLUX_DB,
});

exports.getAllFarmFields = async (req, res) => {
  try {
    let fields = await Field.find({
      farm_id: req.params.farm_id,
      archive: false,
    })
      .sort('-timestamp')
      .select('-__v -timestamp')
      .lean();

    if (!fields) {
      return res.status(404).json({
        status: 'Not Found',
        message: 'No fields found for that farm',
      });
    }

    // Loop through fields and get soil, crop, irrigation and irrigation associated data
    let fieldsWithData = [];
    for (let i = 0; i < fields.length; i++) {
      const soil = await Soil.findOne({
        field_id: fields[i]._id,
        archive: false,
      })
        .select('-timestamp -__v')
        .lean();
      if (soil) {
        fields[i].soil = soil;
      }
      const crop = await Crop.findOne({
        field_id: fields[i]._id,
        archive: false,
      })
        .select('-timestamp -__v')
        .lean();
      if (crop) {
        fields[i].crop = crop;
      }
      const irrigation = await Irrigation.findOne({
        field_id: fields[i]._id,
        archive: false,
      })
        .select('-timestamp -__v')
        .lean();
      if (irrigation) {
        if (irrigation.system_type === 'pivot') {
          const pivot = await Pivot.findOne({
            irrigation_id: irrigation._id,
            archive: false,
          })
            .select('-timestamp -__v')
            .lean();
          if (pivot) {
            for (let key in pivot) {
              irrigation[key] = pivot[key];
            }
          }
          // irrigation.pivot = pivot;
        } else if (irrigation.system_type === 'drip') {
          const drip = await Drip.findOne({
            irrigation_id: irrigation._id,
            archive: false,
          })
            .select('-timestamp -__v')
            .lean();
          if (drip) {
            for (let key in drip) {
              irrigation[key] = drip[key];
            }
          }
          // irrigation.drip = drip;
        } else if (irrigation.system_type === 'sdi') {
          const sdi = await SDI.findOne({
            irrigation_id: irrigation._id,
            archive: false,
          })
            .select('-timestamp -__v')
            .lean();
          if (sdi) {
            for (let key in sdi) {
              irrigation[key] = sdi[key];
            }
          }
          // irrigation.sdi = sdi;
        } else if (irrigation.system_type === 'sprinkler') {
          const sprinkler = await Sprinkler.findOne({
            irrigation_id: irrigation._id,
            archive: false,
          })
            .select('-timestamp -__v')
            .lean();
          if (sprinkler) {
            for (let key in sprinkler) {
              irrigation[key] = sprinkler[key];
            }
          }
          // irrigation.sprinkler = sprinkler;
        } else if (irrigation.system_type === 'furrow') {
          const furrow = await Furrow.findOne({
            irrigation_id: irrigation._id,
            archive: false,
          })
            .select('-timestamp -__v')
            .lean();
          if (furrow) {
            for (let key in furrow) {
              irrigation[key] = furrow[key];
            }
          }
          // irrigation.furrow = furrow;
        }
        fields[i].irrigation = irrigation;
      }
      // push newly generated field to fieldsWithData array
      fieldsWithData.push(fields[i]);
      // get all stats for fields
      // total devices
      let totalDevices = await Device.countDocuments({
        field_id: fields[i]._id,
      }).lean();
      fields[i].totalDevices = totalDevices;
      // temperature & humidity
      let device = await Device.findOne({
        field_id: fields[i]._id,
        device_type: 'air',
      })
        .select('device_type device_topic')
        .lean();
      if (totalDevices > 0) {
        if (!device) {
          fields[i].airtemp = 0;
          fields[i].humidity = 0;
        } else {
          await influx
            .query(
              `SELECT last(uplink_message_decoded_payload_humidity) as humidity, last(uplink_message_decoded_payload_airtemp) as airtemp FROM telegraf.autogen.mqtt_consumer WHERE topic = '${device.device_topic}'`
            )
            .then((result) => {
              if (result.length > 0) {
                fields[i].airtemp = result[0].airtemp;
                fields[i].humidity = result[0].humidity;
              }
            });
        }
      }
    }

    return res.status(200).json({
      status: 'Success',
      result: fields.length,
      body: {
        total_elements: fields.length,
        items: fieldsWithData,
      },
    });
  } catch (err) {
    console.log(err);
    res.status(400).json({
      status: 'Bad Request',
      message: err.message,
    });
  }
};

exports.createField = async (req, res) => {
  try {
    // Get request body and saperate it into variables
    const { name, description, farm_id, location, soil, crop, irrigation } =
      req.body;

    // Create new field
    const fieldData = new Field({ name, description, farm_id, location });
    const field = await fieldData.save();

    // Get Soil parameters and save to collection
    if (soil) {
      const { depth, clay, sand, silt, organic_matter, ph, cec, ec } = soil;
      const soilData = new Soil({
        depth,
        clay,
        sand,
        silt,
        organic_matter,
        ph,
        cec,
        ec,
        field_id: field._id,
      });
      await soilData.save();
    }

    // Get Crops parameters and save to collection
    if (crop) {
      const { crop_type, planting_date, emergence_date, previous_crop } = crop;
      const cropData = new Crop({
        crop_type,
        planting_date,
        emergence_date,
        previous_crop,
        field_id: field._id,
      });
      await cropData.save();
    }
    // Get Irrigation parameters and save to collection
    if (irrigation) {
      const { system_type } = irrigation;
      const irrigationData = new Irrigation({
        system_type,
        field_id: field._id,
      });
      const newIrrigation = await irrigationData.save();

      // Get Irrigation parameters and save to collection
      if (system_type === 'pivot') {
        const { pivot_shape, name, flow_rate, pivot_length, full_run_time } =
          irrigation;
        const pivotData = new Pivot({
          pivot_shape,
          name,
          flow_rate,
          pivot_length,
          full_run_time,
          irrigation_id: newIrrigation._id,
        });
        await pivotData.save();
      } else if (system_type === 'drip') {
        const { flow_rate, actual_area, percent_irrigated } = irrigation;
        const dripData = new Drip({
          flow_rate,
          actual_area,
          percent_irrigated,
          irrigation_id: newIrrigation._id,
        });
        await dripData.save();
      } else if (system_type === 'sdi') {
        const { flow_rate, actual_area, percent_irrigated } = irrigation;
        const SDIData = new SDI({
          flow_rate,
          actual_area,
          percent_irrigated,
          irrigation_id: newIrrigation._id,
        });
        await SDIData.save();
      } else if (system_type === 'sprinkler') {
        const { flow_rate, actual_area, percent_irrigated } = irrigation;
        const sprinklerData = new Sprinkler({
          flow_rate,
          actual_area,
          percent_irrigated,
          irrigation_id: newIrrigation._id,
        });
        await sprinklerData.save();
      } else if (system_type === 'furrow') {
        const { flow_rate, actual_area, percent_irrigated } = irrigation;
        const furrowData = new Furrow({
          flow_rate,
          actual_area,
          percent_irrigated,
          irrigation_id: newIrrigation._id,
        });
        await furrowData.save();
      }
    }

    return res.status(201).json({
      status: 'Success',
      message: 'Field created successfully',
      body: {
        field: field,
      },
    });
  } catch (err) {
    console.log(err);
    res.status(400).json({
      status: 'Bad Request',
      message: err,
    });
  }
};

// Get single field and its related data
exports.getField = async (req, res) => {
  try {
    // Get field data
    let field = await Field.findById(req.params.id)
      .select('-__v -timestamp -location._id')
      .lean();

    if (!field) {
      return res.status(404).json({
        status: 'Not Found',
        message: 'Field not found',
      });
    }

    // Get soil data
    let soil = await Soil.findOne({ field_id: req.params.id, archive: false })
      .select('-__v -timestamp')
      .lean();
    if (soil) {
      field.soil = soil;
    }

    // Get crop data
    let crop = await Crop.findOne({ field_id: req.params.id, archive: false })
      .select('-__v -timestamp')
      .lean();
    if (crop) {
      field.crop = crop;
    }

    // Get irrigation data
    let irrigation = await Irrigation.findOne({
      field_id: req.params.id,
      archive: false,
    })
      .select('-__v -timestamp')
      .lean();

    if (irrigation) {
      // Get irrigation children data
      if (irrigation.system_type === 'pivot') {
        let pivot = await Pivot.findOne({
          irrigation_id: irrigation._id,
          archive: false,
        })
          .select('-__v -timestamp')
          .lean();
        if (pivot) {
          for (let key in pivot) {
            irrigation[key] = pivot[key];
          }
        }

        // irrigation.pivot = pivot;
      } else if (irrigation.system_type === 'drip') {
        let drip = await Drip.findOne({
          irrigation_id: irrigation._id,
          archive: false,
        })
          .select('-__v -timestamp')
          .lean();
        if (drip) {
          for (let key in drip) {
            irrigation[key] = drip[key];
          }
        }

        // irrigation.drip = drip;
      } else if (irrigation.system_type === 'sdi') {
        let sdi = await SDI.findOne({
          irrigation_id: irrigation._id,
          archive: false,
        })
          .select('-__v -timestamp')
          .lean();
        if (sdi) {
          for (let key in sdi) {
            irrigation[key] = sdi[key];
          }
        }

        // irrigation.sdi = sdi;
      } else if (irrigation.system_type === 'sprinkler') {
        let sprinkler = await Sprinkler.findOne({
          irrigation_id: irrigation._id,
          archive: false,
        })
          .select('-__v -timestamp')
          .lean();
        if (sprinkler) {
          for (let key in sprinkler) {
            irrigation[key] = sprinkler[key];
          }
        }

        // irrigation.sprinkler = sprinkler;
      } else if (irrigation.system_type === 'furrow') {
        let furrow = await Furrow.findOne({
          irrigation_id: irrigation._id,
          archive: false,
        })
          .select('-__v -timestamp')
          .lean();
        if (furrow) {
          for (let key in furrow) {
            irrigation[key] = furrow[key];
          }
        }
        // irrigation.furrow = furrow;
      }
      field.irrigation = irrigation;
    }

    // Get all devices in a field
    let devices = await Device.find({
      field_id: req.params.id,
      archive: false,
    })
      .select('name device_topic')
      .lean();
    if (devices) {
      // Get the latest IoT data for each device
      for (let i = 0; i < devices.length; i++) {
        let device = devices[i];
        await influx
          .query(
            `SELECT last(uplink_message_decoded_payload_converted_gndmoist1) as moisture FROM mqtt_consumer WHERE topic = '${device.device_topic}'`
          )
          .then(async (result) => {
            if (result) {
              let moisture = result[0].moisture;
              device.moisture = moisture;
              if (moisture <= 40) {
                device.moisture_status = 'low';
              } else if (moisture >= 40) {
                device.moisture_status = 'high';
              }
            }
          });
      }
      field.devices = devices;
    }

    return res.status(200).json({
      status: 'Success',
      body: field,
    });
  } catch (err) {
    console.log(err);
    res.status(400).json({
      status: 'Bad Request',
      message: err,
    });
  }
};

exports.updateField = async (req, res) => {
  try {
    // Get field data
    const { name, description, farm_id, location } = req.body;
    let newField = await Field.findByIdAndUpdate(
      req.params.id,
      {
        name,
        description,
        farm_id,
        location,
      },
      {
        new: true,
        runValidators: true,
      }
    )
      .select('-__v -timestamp')
      .lean();

    if (!newField) {
      return res.status(404).json({
        status: 'Not Found',
        message: 'Field not found',
      });
    }

    // Get soil data
    if (req.body.soil) {
      const { soil_id, depth, clay, sand, silt, organic_matter, ph, cec, ec } =
        req.body.soil;
      let newSoil = await Soil.findOneAndUpdate(
        soil_id,
        {
          depth,
          clay,
          sand,
          silt,
          organic_matter,
          ph,
          cec,
          ec,
          field_id: req.params.id,
        },
        { new: true, runValidators: true }
      )
        .select('-__v -timestamp')
        .lean();
      newField.soil = newSoil;
    }

    if (req.body.crop) {
      // Get crop data
      const {
        crop_id,
        crop_type,
        planting_date,
        emergence_date,
        previous_crop,
      } = req.body.crop;
      let newCrop = await Crop.findOneAndUpdate(
        crop_id,
        {
          crop_type,
          planting_date,
          emergence_date,
          previous_crop,
          field_id: req.params.id,
        },
        { new: true, runValidators: true }
      )
        .select('-__v -timestamp')
        .lean();
      newField.crop = newCrop;
    }

    if (req.body.irrigation) {
      // Get irrigation data
      const { irrigation_id, system_type } = req.body.irrigation;
      let oldIrrigation = await Irrigation.findById(irrigation_id).select(
        'system_type'
      );
      // Check if requested irrigation system type is same as previous
      if (oldIrrigation.system_type !== system_type) {
        // Delete previous irrigation system type
        const systemType = {
          pivot: Pivot,
          drip: Drip,
          sdi: SDI,
          sprinkler: Sprinkler,
          furrow: Furrow,
        };
        /* console.log({
          message: 'systemType to be deleted',
          oldSystemType: oldIrrigation.system_type,
          newSystemType: system_type,
          systemtype: systemType[oldIrrigation.system_type],
        }); */
        await systemType[oldIrrigation.system_type].findOneAndDelete({
          irrigation_id: irrigation_id,
        });
      }

      let newIrrigation = await Irrigation.findOneAndUpdate(
        irrigation_id,
        {
          system_type,
          field_id: req.params.id,
        },
        { new: true, runValidators: true }
      )
        .select('-__v -timestamp')
        .lean();

      // Get irrigation children data
      if (system_type === 'pivot') {
        const {
          pivot_id,
          pivot_shape,
          name,
          flow_rate,
          pivot_length,
          full_run_time,
        } = req.body.irrigation;

        // Check if updated irrigation system type is same as previous
        if (system_type !== oldIrrigation.system_type) {
          // create new One
          const newPivot = new Pivot({
            pivot_shape,
            name,
            flow_rate,
            pivot_length,
            full_run_time,
            irrigation_id: newIrrigation._id,
          });
          await newPivot.save();
          for (let key in newPivot) {
            newIrrigation[key] = newPivot[key];
          }
          /* console.log({
            message: 'new systemType created',
            oldSystemType: oldIrrigation.system_type,
            newSystemType: system_type,
            systemtype: newPivot,
          }); */
        } else {
          let newPivot = await Pivot.findOneAndUpdate(
            pivot_id,
            {
              pivot_shape,
              name,
              flow_rate,
              pivot_length,
              full_run_time,
              irrigation_id,
            },
            { new: true, runValidators: true }
          )
            .select('-__v -timestamp')
            .lean();
          for (let key in newPivot) {
            newIrrigation[key] = newPivot[key];
          }

          // irrigation.pivot = pivot;
        }
      } else if (system_type === 'drip') {
        const { drip_id, flow_rate, actual_area, percent_irrigated } =
          req.body.irrigation;

        // Check if updated irrigation system type is same as previous
        if (system_type !== oldIrrigation.system_type) {
          // Create new one
          const newDrip = new Drip({
            flow_rate,
            actual_area,
            percent_irrigated,
            irrigation_id: newIrrigation._id,
          });
          await newDrip.save();
          for (let key in newDrip) {
            newIrrigation[key] = newDrip[key];
          }
          /* console.log({
            message: 'new systemType created',
            oldSystemType: oldIrrigation.system_type,
            newSystemType: system_type,
            systemtype: newDrip,
          }); */
        } else {
          let newDrip = await Drip.findOneAndUpdate(
            drip_id,
            { flow_rate, actual_area, percent_irrigated },
            { new: true, runValidators: true }
          )
            .select('-__v -timestamp')
            .lean();
          for (let key in newDrip) {
            newIrrigation[key] = newDrip[key];
          }

          // irrigation.drip = drip;
        }
      } else if (system_type === 'sdi') {
        const { sdi_id, flow_rate, actual_area, percent_irrigated } =
          req.body.irrigation;

        // Check if updated irrigation system type is same as previous
        if (system_type !== oldIrrigation.system_type) {
          // create new one
          const newSDI = new SDI({
            flow_rate,
            actual_area,
            percent_irrigated,
            irrigation_id: newIrrigation._id,
          });
          await newSDI.save();
          for (let key in newSDI) {
            newIrrigation[key] = newSDI[key];
          }
          /* console.log({
            message: 'new systemType created',
            oldSystemType: oldIrrigation.system_type,
            newSystemType: system_type,
            systemtype: newSDI,
          }); */
        } else {
          let newSDI = await SDI.findOneAndUpdate(
            sdi_id,
            { flow_rate, actual_area, percent_irrigated },
            { new: true, runValidators: true }
          )
            .select('-__v -timestamp')
            .lean();
          for (let key in newSDI) {
            newIrrigation[key] = newSDI[key];
          }

          // irrigation.sdi = sdi;
        }
      } else if (system_type === 'sprinkler') {
        const { sprinkler_id, flow_rate, actual_area, percent_irrigated } =
          req.body.irrigation;

        // Check if updated irrigation system type is same as previous
        if (system_type !== oldIrrigation.system_type) {
          // Create new one
          const newSprinkler = new Sprinkler({
            flow_rate,
            actual_area,
            percent_irrigated,
            irrigation_id: newIrrigation._id,
          });
          await newSprinkler.save();
          for (let key in newSprinkler) {
            newIrrigation[key] = newSprinkler[key];
          }
          /* console.log({
            message: 'new systemType created',
            oldSystemType: oldIrrigation.system_type,
            newSystemType: system_type,
            systemtype: newSprinkler,
          }); */
        } else {
          let newSprinkler = await Sprinkler.findOneAndUpdate(
            sprinkler_id,
            {
              flow_rate,
              actual_area,
              percent_irrigated,
            },
            { new: true, runValidators: true }
          )
            .select('-__v -timestamp')
            .lean();
          for (let key in newSprinkler) {
            newIrrigation[key] = newSprinkler[key];
          }

          // irrigation.sprinkler = sprinkler;
        }
      } else if (system_type === 'furrow') {
        const { furrow_id, flow_rate, actual_area, percent_irrigated } =
          req.body.irrigation;

        // Check if updated irrigation system type is same as previous
        if (system_type !== oldIrrigation.system_type) {
          // Create new one
          const newFurrow = new Furrow({
            flow_rate,
            actual_area,
            percent_irrigated,
            irrigation_id: newIrrigation._id,
          });
          await newFurrow.save();
          for (let key in newFurrow) {
            newIrrigation[key] = newFurrow[key];
          }
          /* console.log({
            message: 'new systemType created',
            oldSystemType: oldIrrigation.system_type,
            newSystemType: system_type,
            systemtype: newFurrow,
          }); */
        } else {
          let newFurrow = await Furrow.findOneAndUpdate(
            furrow_id,
            {
              flow_rate,
              actual_area,
              percent_irrigated,
            },
            { new: true, runValidators: true }
          )
            .select('-__v -timestamp')
            .lean();
          for (let key in newFurrow) {
            newIrrigation[key] = newFurrow[key];
          }
          // irrigation.furrow = furrow;
        }
      }
      newField.irrigation = newIrrigation;
    }

    return res.status(200).json({
      status: 'Success',
      body: newField,
    });
  } catch (err) {
    console.log(err);
    res.status(400).json({
      status: 'Bad Request',
      message: err,
    });
  }
};

exports.deleteField = async (req, res) => {
  try {
    // Get field data
    const field = await Field.findById(req.params.id);

    // Delete any devices associated with a field
    const devices = await Device.find({ field_id: req.params.id });
    if (devices) {
      await Device.deleteMany({ field_id: req.params.id });
    }

    // Delete any Soil associated with field
    await Soil.findOneAndDelete({ field_id: field._id });

    // Delete any crop associate with field
    await Crop.findOneAndDelete({ field_id: field._id });

    // Get irrigation associate with field
    const irrigation = await Irrigation.findOne({ field_id: field._id });
    // Delete any system type associated with irrigation
    if (irrigation.system_type === 'pivot') {
      await Pivot.findOneAndDelete({ irrigation_id: irrigation._id });
    } else if (irrigation.system_type === 'drip') {
      await Drip.findOneAndDelete({ irrigation_id: irrigation._id });
    } else if (irrigation.system_type === 'sdi') {
      await SDI.findOneAndDelete({ irrigation_id: irrigation._id });
    } else if (irrigation.system_type === 'sprinkler') {
      await Sprinkler.findOneAndDelete({ irrigation_id: irrigation._id });
    } else if (irrigation.system_type === 'furrow') {
      await Furrow.findOneAndDelete({ irrigation_id: irrigation._id });
    }
    // Delete any irrigation associated with field
    await Irrigation.findOneAndDelete({ field_id: field._id });
    // Delete field
    await Field.findOneAndDelete({ _id: field._id });

    return res.status(200).json({
      status: 'Success',
      message:
        'Field permanent deleted successfully and also associated data with it',
    });
  } catch (err) {
    console.log(err);
    res.status(400).json({
      status: 'Bad Request',
      message: err.message,
    });
  }
};

exports.permanentDelete = async (req, res) => {
  // Get field data
  const field = await Field.findById(req.params.id);

  // Delete any Soil associated with field
  await Soil.findOneAndDelete({ field_id: field._id });

  // Delete any crop associate with field
  await Crop.findOneAndDelete({ field_id: field._id });

  // Get irrigation associate with field
  const irrigation = await Irrigation.findOne({ field_id: field._id });
  // Delete any system type associated with irrigation
  if (irrigation.system_type === 'pivot') {
    await Pivot.findOneAndDelete({ irrigation_id: irrigation._id });
  } else if (irrigation.system_type === 'drip') {
    await Drip.findOneAndDelete({ irrigation_id: irrigation._id });
  } else if (irrigation.system_type === 'sdi') {
    await SDI.findOneAndDelete({ irrigation_id: irrigation._id });
  } else if (irrigation.system_type === 'sprinkler') {
    await Sprinkler.findOneAndDelete({ irrigation_id: irrigation._id });
  } else if (irrigation.system_type === 'furrow') {
    await Furrow.findOneAndDelete({ irrigation_id: irrigation._id });
  }
  // Delete any irrigation associated with field
  await Irrigation.findOneAndDelete({ field_id: field._id });
  // Delete field
  await Field.findOneAndDelete({ _id: field._id });

  return res.status(200).json({
    status: 'Success',
    message:
      'Field permanent deleted successfully and also associated data with it',
  });
};

exports.getAllUserFields = async (req, res) => {
  try {
    const farms = await UserFarm.find({
      user_id: req.params.user_id,
      archive: false,
    })
      // .populate('farm_id', ['name'])
      .select('farm_id')
      .lean();
    if (!farms) {
      return res.status(404).json({
        status: 'Not Found',
        message: 'No farms found for this user',
      });
    }
    const fieldsArr = [];
    for (let i = 0; i < farms.length; i++) {
      const fields = await Field.find({
        farm_id: farms[i].farm_id._id,
        archive: false,
      })
        .select('name')
        .lean();

      if (!fields) {
        return res.status(404).json({
          status: 'Not Found',
          message: 'No fields found for this farm',
        });
      }

      fieldsArr.push(...fields);
      // farms[i].fields = fields;
    }

    return res.status(200).json({
      status: 'Success',
      body: {
        total_elements: fieldsArr.length,
        items: fieldsArr,
      },
    });
  } catch (err) {
    console.log(err);
    res.status(400).json({
      status: 'Bad Request',
      message: err,
    });
  }
};

// exports.createField = factoryHandler.createOne(Field);
exports.getAllFields = factoryHandler.getAllDocs(Field);

////////////////////////////////////////////////
/* Archive 
// Get field data
  const field = await Field.findById(req.params.id);

  // Delete any Soil associated with field
  const soil = await Soil.findOne({ field_id: field._id, archive: false });
  soil.archive = true;
  await soil.save();

  // Delete any crop associate with field
  const crop = await Crop.findOne({ field_id: field._id, archive: false });
  crop.archive = true;
  await crop.save();

  // Get irrigation associate with field
  const irrigation = await Irrigation.findOne({ field_id: field._id });
  // Delete any system type associated with irrigation
  if (irrigation.system_type === 'pivot') {
    const pivot = await Pivot.findOne({
      irrigation_id: irrigation._id,
      archive: false,
    });
    pivot.archive = true;
    await pivot.save();
  } else if (irrigation.system_type === 'drip') {
    const drip = await Drip.findOne({
      irrigation_id: irrigation._id,
      archive: false,
    });
    drip.archive = true;
    await drip.save();
  } else if (irrigation.system_type === 'sdi') {
    const sdi = await SDI.findOne({
      irrigation_id: irrigation._id,
      archive: false,
    });
    sdi.archive = true;
    await sdi.save();
  } else if (irrigation.system_type === 'sprinkler') {
    const sprinkler = await Sprinkler.findOne({
      irrigation_id: irrigation._id,
      archive: false,
    });
    sprinkler.archive = true;
    await sprinkler.save();
  } else if (irrigation.system_type === 'furrow') {
    const furrow = await Furrow.findOne({
      irrigation_id: irrigation._id,
      archive: false,
    });
    furrow.archive = true;
    await furrow.save();
  }
  // Delete any irrigation associated with field
  irrigation.archive = true;
  await irrigation.save();
  // await Irrigation.findOneAndDelete({ field_id: field._id });
  // Delete field
  field.archive = true;
  await field.save();
  // await Field.findOneAndDelete({ _id: field._id });

  return res.status(200).json({
    status: 'Success',
    message: 'Field archived successfully and also associated data with it',
  });
*/
