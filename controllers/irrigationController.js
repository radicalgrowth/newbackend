const Irrigation = require('../models/Irrigation');
const Pivot = require('../models/Pivot');
const Drip = require('../models/Drip');
const SDI = require('../models/SDI');
const Sprinkler = require('../models/Sprinkler');
const Furrow = require('../models/Furrow');
const factoryHandler = require('./factoryHandler');

// Irrigation factory Function
exports.createIrrigation = factoryHandler.createOne(Irrigation);
exports.getAllIrrigations = factoryHandler.getAllDocs(Irrigation);
exports.getIrrigation = factoryHandler.getOne(Irrigation);
exports.updateIrrigation = factoryHandler.updateOne(Irrigation);
exports.deleteIrrigation = factoryHandler.deleteOne(Irrigation);

// Pivot factory functions
exports.createPivot = factoryHandler.createOne(Pivot);
exports.getAllPivots = factoryHandler.getAllDocs(Pivot);
exports.getPivot = factoryHandler.getOne(Pivot);
exports.updatePivot = factoryHandler.updateOne(Pivot);
exports.deletePivot = factoryHandler.deleteOne(Pivot);

// Drip factory functions
exports.createDrip = factoryHandler.createOne(Drip);
exports.getAllDrips = factoryHandler.getAllDocs(Drip);
exports.getDrip = factoryHandler.getOne(Drip);
exports.updateDrip = factoryHandler.updateOne(Drip);
exports.deleteDrip = factoryHandler.deleteOne(Drip);

// SDI factory functions
exports.createSDI = factoryHandler.createOne(SDI);
exports.getAllSDIs = factoryHandler.getAllDocs(SDI);
exports.getSDI = factoryHandler.getOne(SDI);
exports.updateSDI = factoryHandler.updateOne(SDI);
exports.deleteSDI = factoryHandler.deleteOne(SDI);

// Sprinkler factory functions
exports.createSprinkler = factoryHandler.createOne(Sprinkler);
exports.getAllSprinkler = factoryHandler.getAllDocs(Sprinkler);
exports.getSprinkler = factoryHandler.getOne(Sprinkler);
exports.updateSprinkler = factoryHandler.updateOne(Sprinkler);
exports.deleteSprinkler = factoryHandler.deleteOne(Sprinkler);

// Furrow factory functions
exports.createFurrow = factoryHandler.createOne(Furrow);
exports.getAllFurrows = factoryHandler.getAllDocs(Furrow);
exports.getFurrow = factoryHandler.getOne(Furrow);
exports.updateFurrow = factoryHandler.updateOne(Furrow);
exports.DeleteFurrow = factoryHandler.deleteOne(Furrow);
