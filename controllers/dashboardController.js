const UserFarms = require('../models/UserFarm');
const Farm = require('../models/Farm');
const Field = require('../models/Field');
const Device = require('../models/Device');
const Event = require('../models/Event');
const factoryHandler = require('./factoryHandler');
const Influx = require('influx');
const moment = require('moment');

// Influx DB connection
var influx = new Influx.InfluxDB({
  host: process.env.INFLUX_CON_STRING,
  username: process.env.INFLUX_USERNAME,
  password: process.env.INFLUX_PASSWORD,
  port: process.env.INFLUX_PORT,
  database: process.env.INFLUX_DB,
});

exports.getDashboardStats = async (req, res) => {
  try {
    // Implement pagination
    /* let { page, size } = req.query;
    if (!page) page = 1;
    if (!size) size = 10;

    const limit = parseInt(size);
    const skip = (page - 1) * size; */

    // Get all farms of a user
    let deviceData = [];
    const farmsData = await UserFarms.find({ user_id: req.params.user_id })
      .select('farm_id')
      .lean();
    if (!farmsData) {
      return res.status(400).json({
        status: 'Error',
        message: 'User has no farms. Please create a farm',
      });
    }

    // Get all fields of farms
    for (let i = 0; i < farmsData.length; i++) {
      const fields = await Field.find({ farm_id: farmsData[i].farm_id })
        .select('_id')
        .lean();
      if (!fields) {
        return res.status(400).json({
          status: 'Error',
          message: 'Farm has no fields. Please create a field',
        });
      }
      for (let j = 0; j < fields.length; j++) {
        const devices = await Device.find({ field_id: fields[j]._id })
          .select('device_topic')
          .lean();
        if (!devices) {
          return res.status(400).json({
            status: 'Error',
            message: 'Field has no devices',
          });
        }

        for (let k = 0; k < devices.length; k++) {
          await influx
            .query(
              `SELECT last(uplink_message_decoded_payload_converted_gndmoist1) AS gndmoist1, uplink_message_decoded_payload_battery AS battery FROM mqtt_consumer WHERE topic = '${devices[k].device_topic}'`
            )
            .then((result) => {
              const currentTime = new Date();

              const calculatedMinutes =
                (currentTime - result[0].time) / (1000 * 60); // calculated time in minutes
              devices[k].gndMoist1 = result[0].gndmoist1;
              devices[k].battery = result[0].battery;
              devices[k].timeMinutes = calculatedMinutes;
              // devices[k].gndMoist2 = result[0].gndmoist2;
              deviceData.push(devices[k]);
            });
        }
      }
    }
    // console.log({ deviceData });

    let stats = {
      refill: 0,
      optimalDevices: 0,
      fullDevices: 0,
      online: 0,
      offline: 0,
      lowBattery: 0,
    };
    for (let i = 0; i < deviceData.length; i++) {
      // check which device lie in which category
      if (deviceData[i].gndMoist1 <= 40) {
        stats.refill += 1;
      } else if (
        deviceData[i].gndMoist1 >= 40 &&
        deviceData[i].gndMoist1 <= 80
      ) {
        stats.optimalDevices += 1;
      } else if (deviceData[i].gndMoist1 >= 80) {
        stats.fullDevices += 1;
      }
      // check if devices are online or offline by measuring battery level
      if (deviceData[i].timeMinutes < 20) {
        stats.online += 1;
      } else if (deviceData[i].timeMinutes > 18) {
        stats.offline += 1;
      }
      /* if (deviceData[i].battery !== null) {
        stats.online += 1;
      } else if (
        deviceData[i].battery === null ||
        deviceData[i].battery <= 2.5
      ) {
        stats.offline += 1;
      } */
      if (deviceData[i].battery <= 2.8) {
        stats.lowBattery += 1;
      }
    }
    res.status(200).json({
      status: 'Success',
      body: {
        // total_elements: deviceData.length,
        items: stats,
      },
    });
  } catch (err) {
    console.log(err);
    return res.status(400).json({
      status: 'Error',
      message: err.message,
    });
  }
};

exports.getUserEvents = async (req, res) => {
  try {
    const events = await Event.find({
      user_id: req.params.user_id,
      archive: false,
    }).lean();
    if (!events) {
      return res.status(400).json({
        status: 'Error',
        message: 'User has no events. Please create an event',
      });
    }
    return res.status(200).json({
      status: 'Success',
      body: {
        total_elements: events.length,
        items: events,
      },
    });
  } catch (err) {
    console.log(err);
    return res.status(400).json({
      status: 'Error',
      message: err.message,
    });
  }
};

exports.createEvent = factoryHandler.createOne(Event);
exports.getAllEvents = factoryHandler.getAllDocs(Event);
exports.getEvent = factoryHandler.getOne(Event);
exports.updateEvent = factoryHandler.updateOne(Event);
exports.deleteEvent = factoryHandler.deleteOne(Event);

exports.deletePermanently = async (req, res) => {
  try {
    const event = await Event.findByIdAndDelete(req.params.id);
    if (!event) {
      return res.status(400).json({
        status: 'Error',
        message: 'Event not found',
      });
    }
    return res.status(200).json({
      status: 'Success',
      message: 'Event deleted permanently',
    });
  } catch (err) {
    console.log(err);
    return res.status(400).json({
      status: 'Error',
      message: err.message,
    });
  }
};
