const Sensor = require('../models/Sensor');
const factoryHandler = require('./factoryHandler');

exports.createSensor = factoryHandler.createOne(Sensor);
exports.getAllSensor = factoryHandler.getAllDocs(Sensor);
exports.getSensor = factoryHandler.getOne(Sensor);
exports.updateSensor = factoryHandler.updateOne(Sensor);
exports.deleteSensor = factoryHandler.deleteOne(Sensor);
