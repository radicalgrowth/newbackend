const Farm = require('../models/Farm');
const UserFarm = require('../models/UserFarm');
const Field = require('../models/Field');
const Device = require('../models/Device');
const Soil = require('../models/Soil');
const Crop = require('../models/Crop');
const Irrigation = require('../models/Irrigation');
const Drip = require('../models/Drip');
const SDI = require('../models/SDI');
const Sprinkler = require('../models/Sprinkler');
const Furrow = require('../models/Furrow');
const factoryHandler = require('./factoryHandler');

exports.getAllUserFarms = async (req, res) => {
  try {
    const farmsId = await UserFarm.find({
      user_id: req.params.user_id,
      archive: false,
    }).lean();

    if (!farmsId) {
      return res.status(404).json({
        status: 'Not Found',
        message: 'No farms found',
      });
    }
    const farms = [];

    for (let i = 0; i < farmsId.length; i++) {
      const farm = await Farm.findById(farmsId[i].farm_id)
        .select('name')
        .lean();
      farms.push(farm);
    }

    res.status(200).json({
      status: 'Success',
      body: {
        total_elements: farmsId.length,
        items: farms,
      },
    });
  } catch (err) {
    console.log(err);
    res.status(400).json({
      status: 'Bad Request',
      message: err,
    });
  }
};

exports.createFarm = async (req, res) => {
  const farm = await Farm.create({
    name: req.body.name,
  });
  await UserFarm.create({
    user_id: req.body.user_id,
    farm_id: farm._id,
  });
  //   console.log({ farmData: farm, userFarmData: userFarm });

  return res.status(200).json({
    status: 'Success',
    body: farm,
  });
};

exports.getAllFarms = factoryHandler.getAllDocs(Farm);
exports.getFarm = factoryHandler.getOne(Farm);
exports.updateFarm = factoryHandler.updateOne(Farm);
exports.deleteFarm = async (req, res) => {
  try {
    const farm = await Farm.findById(req.params.id).select('_id');
    await UserFarm.findOneAndDelete({ farm_id: farm._id });

    // Get field data
    const field = await Field.find({ farm_id: farm._id }).select('_id');

    for (let i = 0; i < field.length; i++) {
      // Delete any devices associated with a field
      const devices = await Device.find({ field_id: field[i]._id }).select(
        '_id'
      );
      if (devices) {
        await Device.deleteMany({ field_id: field[i]._id });
      }
      // Delete any Soil associated with field
      await Soil.findOneAndDelete({ field_id: field[i]._id });

      // Delete any crop associate with field
      await Crop.findOneAndDelete({ field_id: field[i]._id });

      // Get irrigation associate with field
      const irrigation = await Irrigation.findOne({
        field_id: field[i]._id,
      }).select('_id');
      // Delete any system type associated with irrigation
      if (irrigation.system_type === 'pivot') {
        await Pivot.findOneAndDelete({ irrigation_id: irrigation._id });
      } else if (irrigation.system_type === 'drip') {
        await Drip.findOneAndDelete({ irrigation_id: irrigation._id });
      } else if (irrigation.system_type === 'sdi') {
        await SDI.findOneAndDelete({ irrigation_id: irrigation._id });
      } else if (irrigation.system_type === 'sprinkler') {
        await Sprinkler.findOneAndDelete({ irrigation_id: irrigation._id });
      } else if (irrigation.system_type === 'furrow') {
        await Furrow.findOneAndDelete({ irrigation_id: irrigation._id });
      }
      // Delete any irrigation associated with field
      await Irrigation.findOneAndDelete({ field_id: field[i]._id });
      // Delete field
      await Field.findOneAndDelete({ _id: field[i]._id });
    }

    await Farm.findByIdAndDelete(req.params.id);
    return res.status(200).json({
      status: 'Success',
      message:
        'Farm permanently deleted Successfully and associated data with it',
    });
  } catch (err) {
    console.log(err);
    res.status(400).json({
      status: 'Bad Request',
      message: err,
    });
  }
};

exports.permanentDelete = async (req, res) => {};

///////////////////////////////////////////////
// Archive Farm
/* const farm = await Farm.findById(req.params.id);
farm.archive = true;
farm.save();

const field = Field.find({ farm_id: req.params.id, archive: false }).select(
  '_id'
);

for (let i = 0; i < field.length; i++) {
  field[i].archive = true;
  await field[i].save();

  // Delete any Soil associated with field
  const soil = await Soil.findOne({
    field_id: field[i]._id,
    archive: false,
  }).select('_id');
  soil.archive = true;
  await soil.save();

  // Delete any crop associate with field
  const crop = await Crop.findOne({
    field_id: field[i]._id,
    archive: false,
  }).select('_id');
  crop.archive = true;
  await crop.save();

  // Get irrigation associate with field
  const irrigation = await Irrigation.findOne({
    field_id: field[i]._id,
  }).select('_id');
  // Delete any system type associated with irrigation
  if (irrigation.system_type === 'pivot') {
    const pivot = await Pivot.findOne({
      irrigation_id: irrigation._id,
      archive: false,
    }).select('_id');
    pivot.archive = true;
    await pivot.save();
  } else if (irrigation.system_type === 'drip') {
    const drip = await Drip.findOne({
      irrigation_id: irrigation._id,
      archive: false,
    }).select('_id');
    drip.archive = true;
    await drip.save();
  } else if (irrigation.system_type === 'sdi') {
    const sdi = await SDI.findOne({
      irrigation_id: irrigation._id,
      archive: false,
    }).select('_id');
    sdi.archive = true;
    await sdi.save();
  } else if (irrigation.system_type === 'sprinkler') {
    const sprinkler = await Sprinkler.findOne({
      irrigation_id: irrigation._id,
      archive: false,
    }).select('_id');
    sprinkler.archive = true;
    await sprinkler.save();
  } else if (irrigation.system_type === 'furrow') {
    const furrow = await Furrow.findOne({
      irrigation_id: irrigation._id,
      archive: false,
    }).select('_id');
    furrow.archive = true;
    await furrow.save();
  }
  // Delete any irrigation associated with field
  irrigation.archive = true;
  await irrigation.save();
} */
