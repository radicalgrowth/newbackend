const DeviceAllocation = require("./../models/DeviceAllocation");
const factoryHandler = require("./factoryHandler");

exports.createDeviceAllocation = factoryHandler.createOne(DeviceAllocation);
exports.getAllDeviceAllocation = factoryHandler.getAllDocs(DeviceAllocation);
exports.getDeviceAllocation = factoryHandler.getOne(DeviceAllocation);
exports.updateDeviceAllocation = factoryHandler.updateOne(DeviceAllocation);
exports.deleteDeviceAllocation = factoryHandler.deleteOne(DeviceAllocation);
