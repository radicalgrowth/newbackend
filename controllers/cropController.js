const Crop = require('../models/Crop');
const factoryHandler = require('./factoryHandler');

exports.createCrop = factoryHandler.createOne(Crop);
exports.getAllCrops = factoryHandler.getAllDocs(Crop);
exports.getCrop = factoryHandler.getOne(Crop);
exports.updateCrop = factoryHandler.updateOne(Crop);
exports.deleteCrop = factoryHandler.deleteOne(Crop);
